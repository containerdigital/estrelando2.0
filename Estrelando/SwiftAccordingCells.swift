//
//  SwiftAccordingCells.swift
//  Estrelando
//
//  Created by Renato Mendes on 5/4/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import Foundation

class SwiftAccordionCells {
    private (set) var items = [Item]()
    
    class Item {
        var isHidden: Bool
        var value: AnyObject
        
        init(_ hidden: Bool = true, value: AnyObject) {
            self.isHidden = hidden
            self.value = value
        }
    }
    
    class HeaderItem: Item {
        init (value: AnyObject) {
            super.init(false, value: value)
        }
    }
    
    func append(item: Item) {
        self.items.append(item)
    }
    
    func removeAll() {
        self.items.removeAll()
    }
    
    func expand(headerIndex: Int) {
        self.toogleVisible(headerIndex, isHidden: false)
    }
    
    func collapse(headerIndex: Int) {
        self.toogleVisible(headerIndex, isHidden: true)
    }
    
    private func toogleVisible(headerIndex: Int, isHidden: Bool) {
        
        var index = headerIndex
        
        index = index + 1
        
        while index < self.items.count && !(self.items[index] is HeaderItem) {
            self.items[index].isHidden = isHidden
            
            index = index + 1
        }
    }
}