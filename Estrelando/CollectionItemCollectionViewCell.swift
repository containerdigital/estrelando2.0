//
//  CollectionItemCollectionViewCell.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/9/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class CollectionItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var _scrollView: UIScrollView!
    @IBOutlet weak var _imgView: UIImageView!
    @IBOutlet weak var _count: UILabel!
    @IBOutlet weak var _txtView: UITextView!
    @IBOutlet weak var _countIcon: UILabel!
    @IBOutlet weak var viewBackCount: UIView!
    
    override func awakeFromNib() {
        viewBackCount.layer.cornerRadius = viewBackCount.bounds.width / 2
        viewBackCount.clipsToBounds = true
        
        _countIcon.layer.cornerRadius = _countIcon.layer.bounds.width / 2
        _countIcon.clipsToBounds      = true    
    }

}
