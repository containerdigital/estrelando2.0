//
//  DownloaderDelegate.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Updated by Renato Mendes on 17/03/16
//
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

@objc protocol DownloaderDelegate: class{
    optional func downloaderProgressSucces(contexto:AnyObject?, download:Downloader)
    func downloaderDidFinnish(contexto:AnyObject?,download:Downloader,dados:NSData)
}

//|------------------------------------------
//| Classe para download de data do servidor.
class Downloader: NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate {
    var dataReceived:NSMutableData! = NSMutableData()
    var allSize:Float = 0
    var downloadSize:Float = 0
    var context:AnyObject?
    var download: NSURLSessionDataTask?
    var delegate:DownloaderDelegate?
    
    func initDownload(url:NSURL){
        let req = NSURLRequest(URL: url)
        
        initRequest(req)
    }
    
    func initRequest(req:NSURLRequest){
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        let session = NSURLSession(
            configuration: configuration,
            delegate: self,
            delegateQueue: nil
        )
        
        download = session.dataTaskWithRequest(req)
        
        download!.resume()
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveData data: NSData) {
        dataReceived.appendData(data)
        delegate?.downloaderProgressSucces?(context, download: self)
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        if (error == nil) {//sucess
            
            self.delegate?.downloaderDidFinnish(context, download: self, dados: dataReceived)
            dataReceived = NSMutableData()
            
        } else {//fail
            
            let errorAlert = UIAlertController(title: "!", message: "Não podemos iniciar o download.", preferredStyle: UIAlertControllerStyle.Alert)
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                // ...
            }
            errorAlert.addAction(OKAction)
            
            
            let rootViewController : UIViewController = (UIApplication.sharedApplication().keyWindow?.rootViewController)!
            rootViewController.presentViewController(errorAlert, animated: true) {
                
            }
            
        }
    }
    
    //Geting the all size of archive
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, didReceiveResponse response: NSURLResponse, completionHandler: (NSURLSessionResponseDisposition) -> Void) {
        
        dataReceived = NSMutableData()
        allSize = Float(response.expectedContentLength)
        
        completionHandler(NSURLSessionResponseDisposition.Allow)
    }
    
    func URLSession(session: NSURLSession, dataTask: NSURLSessionDataTask, willCacheResponse proposedResponse: NSCachedURLResponse, completionHandler: (NSCachedURLResponse?) -> Void) {

        var userData = NSMutableDictionary()
        
        if let userInfo = proposedResponse.userInfo{
            userData = NSMutableDictionary(dictionary: userInfo)
        }
        
        let mutableData = NSMutableData(data: proposedResponse.data)
        let storagePolicy:NSURLCacheStoragePolicy = NSURLCacheStoragePolicy.AllowedInMemoryOnly
        
        let proposedResponse = NSCachedURLResponse(response: proposedResponse.response, data: mutableData, userInfo: userData as [NSObject: AnyObject], storagePolicy: storagePolicy)

        completionHandler(proposedResponse)
    }
    
}
