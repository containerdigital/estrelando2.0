//
//  RankingViewController.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/8/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit
import SDWebImage

class RankingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var _title: UILabel!
    @IBOutlet weak var _date: UILabel!
    @IBOutlet weak var _headerCarousel: UICollectionView!
    @IBOutlet weak var _collectionCarousel: UICollectionView!
    @IBOutlet weak var _contentView: UIView!
    
    var content: Content?
    var ranking: Ranking! = Ranking()
    var categorie: String?
    
    var imagesCache = NSMutableDictionary()
    
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //NAVIGATION
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo-estrelando"))
     
        //CONTENT
        self._title.text = self.content?.title?.string
        self._title.textColor = ColorForCategorie().colorForCategorie(categorie!)
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            self._title.font = UIFont(name: "Roboto-Medium", size: 16)
        }
        
        //CAROUSEL
        let itemCarousel = UINib(nibName: "CarouselItemCollectionViewCell", bundle: nil)
        self._headerCarousel.registerNib(itemCarousel, forCellWithReuseIdentifier: "item")
        
        let flowLayoutHeader = UICollectionViewFlowLayout()
        flowLayoutHeader.itemSize = CGSize(width: 110, height: 75)
        flowLayoutHeader.minimumLineSpacing = 12
        flowLayoutHeader.scrollDirection = .Horizontal
        flowLayoutHeader.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15)
        
        self._headerCarousel.collectionViewLayout = flowLayoutHeader
        self._headerCarousel.tag = 5
        self._headerCarousel.delegate = self
        self._headerCarousel.dataSource = self
        
        //COLLECTION
        let itemCollection = UINib(nibName: "CollectionItemCollectionViewCell", bundle: nil)
        self._collectionCarousel.registerNib(itemCollection, forCellWithReuseIdentifier: "itemCollection")
        
        let flowLayoutCollection = UICollectionViewFlowLayout()
        flowLayoutCollection.itemSize = CGSize(width: self.view.bounds.width, height: self._collectionCarousel.bounds.height)
        flowLayoutCollection.minimumLineSpacing = 0
        flowLayoutCollection.scrollDirection = .Horizontal
        
        self._collectionCarousel.collectionViewLayout = flowLayoutCollection
        self._collectionCarousel.tag = 10
        self._collectionCarousel.delegate = self
        self._collectionCarousel.dataSource = self
        
        //API CALL
        let url = "http://app.estrelando.com.br/v1/ranking/\(content!.id!)"
        
        let promise = EstrelandoAPI.rankingInside(url)
        promise.then { () -> Void in
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.ranking = promise.Result as! Ranking
                
                self._date.text = self.ranking._publishedAt
                
                self._headerCarousel?.reloadData()
                self._collectionCarousel.reloadData()
            }
        }
    }
    
    
    // MARK: - COLLECTION VIEW DELEGATE
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ranking._arrImage.count
    }
    
    
    // MARK: - COLLECTION VIEW DATASOURCE
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let item = ranking._arrImage[indexPath.row+1]
        
        switch collectionView.tag {
        case 5:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("item", forIndexPath: indexPath) as! CarouselItemCollectionCell
            
            cell._shadow.text = "\(indexPath.row + 1)º"
            
            //Download de imagem
            if (item._imageURL != nil && item._idImage != nil) {
                if let url = NSURL(string: item._imageURL!) {
                    cell._image.sd_setImageWithURL(url, placeholderImage: UIImage(named: "default_image"), options: SDWebImageOptions.RefreshCached)
                }
            }
            
            return cell
        case 10:
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("itemCollection", forIndexPath: indexPath) as! CollectionItemCollectionViewCell
            
            cell._count.text = "\(indexPath.row + 1)"
            cell._count.textColor = ColorForCategorie().colorForCategorie(categorie!)
            
            cell._countIcon.text = "\(indexPath.row + 1)º"
            cell._countIcon.textColor = ColorForCategorie().colorForCategorie(categorie!)
            
            cell._txtView.attributedText = item._description
            cell._txtView.textColor = UIColor.lightGrayColor()
            
            cell._txtView.scrollEnabled = false
            var size:CGFloat = cell._count.frame.height
            size = size + cell._imgView.frame.height
            size = size + cell._txtView.contentSize.height
            size = size + 10
            
            cell._scrollView.contentSize = CGSize(width: cell._txtView.frame.width, height: size)
            cell._txtView.sizeToFit()
            cell._txtView.layoutIfNeeded()
            
            cell._imgView.hidden = true
            
            //Download de imagem
            if (item._imageURL != nil && item._idImage != nil) {
                if let url = NSURL(string: item._imageURL!) {
                    cell._imgView.sd_setImageWithURL(url, placeholderImage: nil, options: SDWebImageOptions.RefreshCached) {
                        img, error, _, _ in
                        
                        cell._imgView.hidden = false
                    }
                }
            }
            
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        switch collectionView.tag {
        case 5:
            self._collectionCarousel.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
            
        case 10:
            break
        default:
            break
        }
    }
    

    // MARK: - IBACTIONS
    
    // NAVIGATION ACTIONS
    
    @IBAction func rightButtonAction(sender: AnyObject) {
        //ação para chamar view de comentários.
    }
    
    @IBAction func dismissView(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // FOOTER VIEW ACTIONS
    
    @IBAction func shareAction(sender: AnyObject) {
        if let textToShare = self.ranking._link {
            let objectsToShare : Array = [textToShare];
            
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }

}
