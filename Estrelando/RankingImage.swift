//
//  RankingImage.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/8/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import Foundation

class RankingImage: NSObject {
    var _type: String?
    var _description: NSAttributedString?
    var _credits: String?
    var _order: NSNumber?
    var _imageURL: String?
    var _idImage: NSNumber?
    
    override init() {
        _type = nil
        _description = nil
        _credits = nil
        _order = nil
        _imageURL = nil
        _idImage = nil
    }
    
    init(dictionary: NSDictionary) {
        if let type = dictionary["type"] as? String {
            _type = type
        }
        
        if let description = dictionary["description"] as? String {
            _description = String.converString(description)
        }
        
        if let credits = dictionary["credits"] as? String {
            _credits = credits
        }
        
        if let order = dictionary["order"] as? NSNumber {
            _order = order
        }
        
        if let dict = dictionary["Files"] as? NSDictionary {
            if let url = dict["url"] as? String {
                _imageURL = url
            }
        }
        
        if let dict = dictionary["Files"] as? NSDictionary {
            if let id = dict["id"] as? NSNumber {
                _idImage = id
            }
        }
    }
    
}