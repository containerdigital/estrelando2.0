//
//  HomeViewController.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/24/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SDWebImage

class HomeViewController: BaseRootViewController, UICollectionViewDataSource, UICollectionViewDelegate, GADBannerViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerGA: DFPBannerView!
    
    let reuseIdentifier = "cell"
    
    var pageViewController : UIPageViewController!
    var pageTitles : NSArray!
    var pageImages : NSArray!
    var pageCategories : NSArray!
    var content = Array<MainHomeContent>()
    var feature = Array<MainHomeContent>()
    var headerAux = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Declare FlowLayout
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.headerReferenceSize = CGSizeMake(self.collectionView!.frame.size.width, 300)
        
        // Declare view top
        collectionView?.registerClass(TopCollectionViewCell.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "celula")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        //BANNER CONFIG
        self.bannerGA.adUnitID = "/10646962/BannerAPP"
        self.bannerGA.rootViewController = self
        self.bannerGA.delegate = self
        //print("Google Mobile Ads SDK version: \(GADRequest.sdkVersion())")
        
        let request = DFPRequest()
        //request.testDevices = [kGADSimulatorID, "d6673b9ef77787624f0e2bcd0d7c1cbe", "b71cec12b25d0eb5bc5c89a7dbbd99b3"]
        self.bannerGA.loadRequest(request)
        
        // Call data home
        self.loadData()
    }
    
    func loadData() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let promise = EstrelandoAPI.homeHelper()
        promise.then { () -> Void in
            NSOperationQueue.mainQueue().addOperationWithBlock {
                
                for item in promise.Result as! Array<MainHomeContent> {
                    
                 if item.contentType == "news"{
                    if item.type != "MD" {
                        
                        if item.type == "DT1" || item.type == "DT2" {
                            
                            if item.title == nil{
                                
                                item.title = item.fullText
                                
                            } else {
                                
                                item.title = item.title
                            }
                            
                            
                        }
                        self.content.append(item)
                    } else {
                        self.feature.append(item)
                    }
                  }
                    
                }
                
                self.collectionView?.reloadData()
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
        }
    }
    
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.content.count % 2 == 1{
            return self.content.count - 1
        } else {
            return self.content.count
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ContentCollectionViewCell
        
        let item = self.content[indexPath.row]
        
        let label = ColorForCategorie().labelForCategorie(item.categories!)
        cell.labelCategoria.text = label.text
        cell.labelCategoria.textColor = label.textColor
        cell.labelCategoria.backgroundColor = label.backgroundColor
        cell.labelCategoria.font = UIFont(name: "Roboto-Bold", size: 14.0)
        
        cell.labelMateria.text = item.title?.string
        
        //Download de imagem
        if (item.foto1 != nil && item.id != nil) {
            if let url = NSURL(string: item.foto1!) {
                cell.imageViewCell.sd_setImageWithURL(url, placeholderImage: UIImage(named: "default_image"), options: SDWebImageOptions.RefreshCached) {
                    img, error, _, _ in
                    
                    item.picture = img
                }
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if (kind == UICollectionElementKindSectionHeader) {
            if let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "top", forIndexPath: indexPath) as? TopCollectionViewCell {
                
                if self.content.count > 0 {
                    header.setData(self.feature, view: self)
                }
                
                return header
            }
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if (self.view.frame.size.width > 375) {
            return CGSize(width: self.view.frame.size.width/2 - 1, height: 231);
        } else {
            return CGSize(width: self.view.frame.size.width/2 - 1, height: 231);
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let item = self.content[indexPath.row]
        if let type = item.contentType {
            switch(type){
                
            case "news":
                if let tela = self.storyboard?.instantiateViewControllerWithIdentifier("internaNotas") as? InternaViewController {
                    item.type = item.contentType
                    tela.content = item
                    tela.stringCategory = item.categories
                    self.navigationController?.pushViewController(tela, animated: true)
                }
                
                break
            case "gallery":
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("galleryViewController") as? GalleryViewController {
                    vc.content = item
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                break
            case "ranking":
                if let tela = self.storyboard?.instantiateViewControllerWithIdentifier("rankingViewController") as? RankingViewController {
                    tela.content = item
                    tela.categorie = item.categories
                    self.navigationController?.pushViewController(tela, animated: true)
                }
                
                break
            case "profiles":
                break
            default:
                break
            }
        }
    }
    
    
    // MARK: - DFP DELEGATES
    func adViewDidReceiveAd(bannerView: GADBannerView!) {
        
    }
    
    func adView(bannerView: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        print(error)
        self.bannerGA.hidden = true
    }
    
}
