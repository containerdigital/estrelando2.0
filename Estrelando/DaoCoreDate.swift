//
//  DaoCoreDate.swift
//  KindMe
//
//  Created on 15/07/15.
//  Copyright (c) 2015 KindMe. All rights reserved.
//

import CoreData

@objc(DaoCoreDataDelegate)
protocol DaoCoreDataDelegate{
    func entityObject() -> AnyObject
    func entityDescription() -> NSEntityDescription
    func fetchRequest() -> NSFetchRequest
    func insert(object : AnyObject)->Bool
    optional func select() -> AnyObject
    optional func select(id : String) -> AnyObject
    optional func delete(object : AnyObject) -> Bool
    optional func update(object : AnyObject)
}
class DaoCoreDate{
   
    private var dao : DaoFactoryCoreData?
    init(){
        dao = DaoFactoryCoreData()
    }
    
    func shared() -> DaoFactoryCoreData{
        return self.dao!
    }
    
}
