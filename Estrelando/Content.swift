//
//  MateriaClass.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class Content: NSObject{

    var id : NSNumber?
    var categories : String?
    var foto1 : String?
    var postDate : String?
    var summary : String?
    var title : NSMutableAttributedString?
    var type : String?
    var contentType : String?
    var picture: UIImage?
    var link: String?
    var postText:NSMutableAttributedString?
    var fullText : NSMutableAttributedString?
    let strongSelf = self
    
    override init() {
        
    }
    
    init(link:String){
        super.init()
        self.link = link
    }
    
    init(dictionary:NSDictionary){
        super.init()
        self.initAllKeys(dictionary)
    }
    
    func initAllKeys(dictionary:NSDictionary){
        
        if let id = dictionary.objectForKey("id") as? NSNumber {
            self.id = id
        }
        
        if let categories = dictionary.objectForKey("categories") as? String {
            self.categories = categories
        }
        
        if let file = dictionary.objectForKey("Files") as? NSDictionary {
            if let foto1 = file.objectForKey("url") as? String {
                self.foto1 = foto1 //.resizeImage("230x220")
            }
        } else if let file = dictionary.objectForKey("File") as? NSDictionary {
            if let foto1 = file.objectForKey("url") as? String {
                self.foto1 = foto1 //.resizeImage("230x220")
            }
        }
        
        if let url = dictionary.objectForKey("url") as? String {
            self.link = url
        } else if let link = dictionary.objectForKey("link") as? String {
            self.link = link
        }
        
        if let postDate = dictionary.objectForKey("publishedAt") as? String {
            let date : NSDate = NSDate.initFromString(postDate, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
            self.postDate = date.getDate()
        }
        
        if let summary = dictionary.objectForKey("summary") as? String {
            self.summary = summary
        }
        
        if let title = dictionary.objectForKey("title") as? String {
            self.title = String.converString(title)
        }
        
        if let postText = dictionary.objectForKey("postText") as? String {
            self.postText = String.converString(postText)
        }
        
        if let fullText = dictionary.objectForKey("fullText") as? String {
            self.fullText = String.converString(fullText)
        }
        
        if let type = dictionary.objectForKey("type") as? String{
            self.type = type
        }
        
        if let contentType = dictionary.objectForKey("contentType") as? String {
            self.contentType = contentType
        }

    }
    
    func matchesForRegexInText(regex: String!, text: String!) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matchesInString(text,
                options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substringWithRange($0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func toGallery() -> Gallery {
        if let link = self.link{
        let gallery = Gallery(link: link)
        
            return gallery
        }
        
        return Gallery(link: self.link!)
    }
    
    func fullContent() -> Promise {
        let promise = Promise()
        let req = ServerHelper()
        
        if let id = self.id  {
            let ret:Promise = req.request("http://app.estrelando.com.br/v1/\(self.type!)/\(id)")
            
            ret.then { () -> Void in
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    if let res = ret.Result as? NSData?{
                        do {
                            let json = try NSJSONSerialization.JSONObjectWithData(res!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                            
                            self.initAllKeys(json)
                            
                            promise.resolv(self)
                        } catch {
                            promise.rejects("Error")
                        }
                    }
                }
            }
        } else {
            promise.rejects("Error")
        }

        return promise
    }

}
