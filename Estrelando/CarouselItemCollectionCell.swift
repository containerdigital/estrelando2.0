//
//  CarouselItemCollectionCell.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/9/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class CarouselItemCollectionCell: UICollectionViewCell {

    @IBOutlet weak var _image: UIImageView!
    @IBOutlet weak var _shadow: UILabel!
    
    override func awakeFromNib() {
        
    }
    
}
