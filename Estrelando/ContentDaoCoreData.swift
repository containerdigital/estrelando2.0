//
//  UserDaoCoreData.swift
//  KindMe
//
//  Created on 15/07/15.
//  Copyright (c) 2015 KindMe. All rights reserved.
//

import CoreData

class ContentDaoCoreData: DaoCoreDate , DaoCoreDataDelegate {
    
    private let className = "Content"
    private var error : NSError?
    
    @objc func entityObject() -> AnyObject{
        return ContentDto(entity: entityDescription(), insertIntoManagedObjectContext: shared().getContext())
    }
    
    @objc func entityDescription() -> NSEntityDescription{
        return NSEntityDescription.entityForName(className, inManagedObjectContext: shared().getContext())!
    }
    
    @objc func fetchRequest() -> NSFetchRequest {
        let fetchRequest = NSFetchRequest(entityName: className)
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
   
    @objc func insert(object: AnyObject) -> Bool {
        do {
            //        println("\(object)")
            try shared().getContext().save()
            return true
        } catch _ {
            return false
        }
    }
    
    @objc func select() -> AnyObject {
        shared().setFetch(fetchRequest())
        
        var error : NSError?
        let result: [AnyObject]?
        do {
            result = try shared().getContext().executeFetchRequest(shared().getFetch())
        } catch let error1 as NSError {
            error = error1
            result = nil
        }
        
        if (error != nil){
            print("\(error)")
            return []
        }else{
            let results : [ContentDto] = result as! [ContentDto]
            return results
        }
    }
    
    @objc func select(id: String) -> AnyObject {
        let predicate = NSPredicate(format: "id = %@", id)
        
        shared().setFetch(fetchRequest())
        shared().getFetch().predicate = predicate
        
        var error : NSError?
        let result: [AnyObject]?
        do {
            result = try shared().getContext().executeFetchRequest(shared().getFetch())
        } catch let error1 as NSError {
            error = error1
            result = nil
        }
        
        if (error != nil){
            print("\(error)")
            return []
        }else{
            let results : [ContentDto] = result as! [ContentDto]
            return results
        }
    }
    
   @objc func delete(object: AnyObject) -> Bool {
        let managed = object as! NSManagedObject
        shared().getContext().deleteObject(managed)
        
        do {
            try shared().getContext().save()
            return true
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error!.userInfo)")
            return false
        }
    }
    
    @objc func update(object: AnyObject) {
        do {
            try shared().getContext().save()
            print("update is complete")
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error!.userInfo)")
        }
    }
}
