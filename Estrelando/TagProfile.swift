//
//  TagProfile.swift
//  Estrelando
//
//  Created by iOS Developer on 30/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class TagProfile: NSObject {
    
    var id : NSNumber?
    var title : String?
    var slug : String?
    var taxonomy : String?
    var job : String?
    var birthday : String?
    var death : String?
    var profileType : String?
    var createdAt : String?
    var updatedAt : String?
    var deletedAt : String?
    var fileId : NSNumber?
    var arrContent : [Content]? = [Content]()
    var file : NSArray?
    var link: String?
    
    override init() {
        id = nil
        title = nil
        slug = nil
        taxonomy = nil
        job = nil
        birthday = nil
        death = nil
        profileType = nil
        createdAt = nil
        updatedAt = nil
        deletedAt = nil
        fileId = nil
        arrContent = [Content]()
        file = NSArray()
        link = nil
    }
    
    init(dictionary:NSDictionary){
        super.init()
        
        if let id = dictionary.objectForKey("id") as? NSNumber {
            self.id = id
        }
        
        if let title = dictionary.objectForKey("title") as? String {
            self.title = title
        }
        
        if let slug = dictionary.objectForKey("slug") as? String {
            self.slug = slug
        }
        
        if let job = dictionary.objectForKey("job") as? String {
            self.job = job
        }
        
        if let birthday = dictionary.objectForKey("profileDate") as? String {
            let date : NSDate = NSDate.initFromString(birthday, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
            self.birthday = date.getDate()
        }
        
        if let death = dictionary.objectForKey("profileDeathDate") as? String {
            let date : NSDate = NSDate.initFromString(death, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
            self.death = "-\(date.getDate())"
        }
        
        if let profileType = dictionary.objectForKey("profileType") as? String {
            self.profileType = profileType
        }
        
        if let createdAt = dictionary.objectForKey("createdAt") as? String {
            let date : NSDate = NSDate.initFromString(createdAt, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
            self.createdAt = date.getDate()
        }
        
        if let updatedAt = dictionary.objectForKey("updatedAt")as? String {
            let date : NSDate = NSDate.initFromString(updatedAt, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
            self.updatedAt = date.getDate()
        }
        
        if let deletedAt = dictionary.objectForKey("deletedAt") as? String {
            let date : NSDate = NSDate.initFromString(deletedAt, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
            self.deletedAt = date.getDate()
        }
        
        if let fileId = dictionary.objectForKey("FileId") as? NSNumber {
            self.fileId = fileId
        }
        
        if let content = dictionary.objectForKey("content") as? NSArray {
            for i in 0 ..< content.count {
                let content = Content(dictionary: content[i] as! NSDictionary)
                self.arrContent?.append(content)
                print("CONTENT",self.arrContent)
            }
        }
        
        if let file = dictionary.objectForKey("File") as? NSArray {
            self.file = file
        }
    
        if let link = dictionary.objectForKey("link") as? String {
            self.link = link
        }
        
    }
    
    func mergeTags(tag:TagProfile){
        for i in 0  ..< tag.arrContent!.count {
            self.arrContent?.append(tag.arrContent![i] as Content)
        }
    }
}
