//
//  ProfilesCollectionViewCell.swift
//  Estrelando
//
//  Created by Renato Mendes on 5/5/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

protocol ProfileCollectionCellDelegate {
    func saveOrDelete(content: Content, indexPath: NSIndexPath)
}

class ProfilesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var readLatter: UIButton!
    
    let daoCoreData = ContentDaoCoreData()
    
    var content : Content!
    var indexPath : NSIndexPath!
    
    var delegate : ProfileCollectionCellDelegate?
    
    override func awakeFromNib() {
        
    }
    
    func setData(content: Content, indexPath: NSIndexPath) {
        self.content   = content
        self.indexPath = indexPath
        
        title.text     = content.title?.string
        date.text      = content.postDate
        imgView?.image = UIImage(named: "Gray_background")
        
        setReadLatterEnable()
    }
    
    private func setReadLatterEnable() {
        switch self.content.type! {
        case "news":
            readLatter.hidden = false
            
            if let result = daoCoreData.select(content.id!.stringValue) as? [ContentDto] {
                if result.count > 0 {
                    readLatter.setImage(UIImage(named: "bt_read_latter_focused"), forState: .Normal)
                    break
                }
            }
            readLatter.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
        default:
            readLatter.hidden = true
            readLatter.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
            
        }
    }
    
    @IBAction func saveOrDelete(sender: AnyObject) {
        self.delegate?.saveOrDelete(content, indexPath: indexPath)
    }
}
