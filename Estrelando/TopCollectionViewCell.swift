//
//  TopCollectionViewCell.swift
//  Estrelando
//
//  Created by iOS Developer on 09/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SDWebImage

private let IDENTIFIER = "item"

class TopCollectionViewCell: UICollectionReusableView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var rainbow: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var results = Array<Content>()
    
    var view: UIViewController?
    
    func setData(results: Array<Content>, view: UIViewController) {
        self.view = view
        
        //COLLECTION
        self.results = results
        self.collectionView.reloadData()
        
        //PAGE CONTROL
        self.pageControl.numberOfPages = results.count
        self.pageControl.currentPageIndicatorTintColor = UIColor.darkGrayColor()
        self.pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.results.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier(IDENTIFIER, forIndexPath: indexPath) as? TopCollectionViewDetailCell {
            
            let item = results[indexPath.row]
            
            cell.title.text = item.fullText?.string
            cell.title.textColor = ColorForCategorie().colorForCategorie(item.categories!)
            
            let category = ColorForCategorie().labelForCategorie(item.categories!)
            cell.category.text = "   " + category.text!.uppercaseString
            cell.category.textColor = category.textColor
            cell.category.backgroundColor = category.backgroundColor
            
            //Download de imagem
            if (item.foto1 != nil && item.id != nil) {
                if let url = NSURL(string: item.foto1!) {
                    cell.imgView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "pattern_BG"), options: SDWebImageOptions.RefreshCached) {
                        img, error, _, _ in
                        
                        item.picture = img
                    }
                }
            }
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.view!.view.frame.size.width, height: 330);
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let item = self.results[indexPath.row]
        if let type = item.contentType {
            switch(type){
                
            case "news":
                if let tela = self.view!.storyboard?.instantiateViewControllerWithIdentifier("internaNotas") as? InternaViewController {
                    item.type = item.contentType
                    item.title = item.fullText
                    tela.content = item
                    tela.stringCategory = item.categories
                    self.view!.navigationController?.pushViewController(tela, animated: true)
                }
                break
            case "gallery":
                if let vc = self.view!.storyboard?.instantiateViewControllerWithIdentifier("galleryViewController") as? GalleryViewController {
                    item.title = item.fullText
                    vc.content = item
                    
                    self.view!.navigationController?.pushViewController(vc, animated: true)
                }
                
                break
            case "ranking":
                if let tela = self.view!.storyboard?.instantiateViewControllerWithIdentifier("rankingViewController") as? RankingViewController {
                    item.title = item.fullText
                    tela.content = item
                    tela.categorie = item.categories
                    self.view!.navigationController?.pushViewController(tela, animated: true)
                }
                break
            case "profiles":
                break
            default:
                break
            }
        }
    }

    // MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let pageWidth = CGRectGetWidth(scrollView.frame)
        pageControl.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
    }
    
}
