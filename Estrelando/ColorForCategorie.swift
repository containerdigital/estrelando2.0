//
//  LabelForCategorie.swift
//  Estrelando
//
//  Created by Renato Mendes on 3/15/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class ColorForCategorie {
    
    // Retorna o label com as cores da categoria atual.
    func labelForCategorie(categorie: String) -> UILabel {
        
        let label : UILabel = UILabel()
        label.text = "  \(categorie.uppercaseString)"
        label.textColor = UIColor.whiteColor()
        
        switch (categorie) {
            
        case "celebridades":
            label.backgroundColor = UIColor(red: 251.0/255, green: 0.0/255, blue: 57.0/255, alpha: 1.0)
            
            break
            
        case "estilo":
            label.backgroundColor = UIColor(red: 120.0/255, green: 0.0/255, blue: 213.0/255, alpha: 1.0)
            
            break
            
        case "realities":
            label.backgroundColor = UIColor(red: 230.0/255, green: 106.0/255, blue: 0.0/255, alpha: 1.0)
            
            break
            
        case "series":
            label.backgroundColor = UIColor(red: 13.0/255, green: 100.0/255, blue: 205.0/255, alpha: 1.0)
            
            break
            
        case "teen":
            label.backgroundColor = UIColor(red: 17.0/255, green: 124.0/255, blue: 28.0/255, alpha: 1.0)
            
            break
            
        case "videos":
            label.backgroundColor = UIColor(red: 253/255, green: 190/255, blue: 44/255, alpha: 1.0)
            
            break
            
        default:
            
            break
        }
        
        return label
    }
    
    // Retorna o label com as cores da categoria atual.
    func textColorForCategorie(categorie: String) -> UILabel {
        
        let label : UILabel = UILabel()
        label.text = "  \(categorie.uppercaseString)"
        label.textColor = UIColor.whiteColor()
        
        switch (categorie) {
            
        case "celebridades":
            label.textColor = UIColor(red: 251.0/255, green: 0.0/255, blue: 57.0/255, alpha: 1.0)
            
            break
            
        case "estilo":
            label.textColor = UIColor(red: 120.0/255, green: 0.0/255, blue: 213.0/255, alpha: 1.0)
            
            break
            
        case "realities":
            label.textColor = UIColor(red: 230.0/255, green: 106.0/255, blue: 0.0/255, alpha: 1.0)
            
            break
            
        case "séries":
            label.textColor = UIColor(red: 13.0/255, green: 100.0/255, blue: 205.0/255, alpha: 1.0)
            
            break
            
        case "teen":
            label.textColor = UIColor(red: 17.0/255, green: 124.0/255, blue: 28.0/255, alpha: 1.0)
            
            break
            
        case "videos":
            label.textColor = UIColor(red: 253/255, green: 190/255, blue: 44/255, alpha: 1.0)
            
            break
            
        default:
            
            break
        }
        
        return label
    }
    
    func colorForCategorie(categorie: String) -> UIColor {
        switch (categorie) {
            
        case "celebridades":
            return UIColor(red: 251.0/255, green: 0.0/255, blue: 57.0/255, alpha: 1.0)
            
        case "estilo":
            return UIColor(red: 120.0/255, green: 0.0/255, blue: 213.0/255, alpha: 1.0)

        case "realities":
            return UIColor(red: 230.0/255, green: 106.0/255, blue: 0.0/255, alpha: 1.0)

        case "séries":
            return UIColor(red: 13.0/255, green: 100.0/255, blue: 205.0/255, alpha: 1.0)

        case "teen":
            return UIColor(red: 17.0/255, green: 124.0/255, blue: 28.0/255, alpha: 1.0)
            
        case "videos":
            return UIColor(red: 253/255, green: 190/255, blue: 44/255, alpha: 1.0)
            
        default:
            return UIColor.blackColor()
        }
    }

}