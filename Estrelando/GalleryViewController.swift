//
//  GalleryViewController.swift
//  Estrelando
//
//  Created by Thiago Harada on 6/10/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit
import SDWebImage

class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var shareButton: UIButton!
    
    var content:Content?
    var gallery: GalleryModel?
    var arrPhotos = [PhotoModel]()
    let cachePicture = NSMutableDictionary()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //CONTENT
        self.titleLabel.text = content?.title?.string
        
        print("URL: ")

        
        //NAVIGATION
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo-estrelando"))
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            self.titleLabel.font = UIFont(name: "Roboto-Medium", size: 16)
        }
        
        //COLLECTION
        let itemCollection = UINib(nibName: "GalleryCollectionViewCell", bundle: nil)
        self.galleryCollectionView.registerNib(itemCollection, forCellWithReuseIdentifier: "itemGallery")
        
        let flowLayoutCollection = UICollectionViewFlowLayout()
        flowLayoutCollection.itemSize = CGSize(width: self.view.bounds.width, height: self.galleryCollectionView.bounds.height)
        flowLayoutCollection.minimumLineSpacing = 0
        flowLayoutCollection.scrollDirection = .Horizontal
        
        self.galleryCollectionView.collectionViewLayout = flowLayoutCollection
        self.galleryCollectionView.delegate = self
        self.galleryCollectionView.dataSource = self
        
        let promise : Promise
        let url = "http://app.estrelando.com.br/v1/gallery/\(self.content!.id!)"
        
        
        
        
        promise = EstrelandoAPI.galleryInside(url)
        promise.then { () -> Void in
            NSOperationQueue.mainQueue().addOperationWithBlock {
                if let gallery = promise.Result as? GalleryModel {
                    self.gallery = gallery
                    self.arrPhotos = gallery.arrPhotos!
                    
                    //formatar data
                    let date : NSDate = NSDate.initFromString(gallery.publishedAt!, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
                    self.dateLabel.text = "\(date.getDate())"
                    
                    self.galleryCollectionView.reloadData()
                }
            }
            
        }
    }
    
    @IBAction func dismissView(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func shareAction(sender: AnyObject) {
        
        if let textToShare = self.gallery?._link {
            let objectsToShare : Array = [textToShare];
            
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
        
    }
    
    
    // MARK: - Collection View Delegate
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let num: Int = self.arrPhotos.count{
            return num
        } else {
            return 0
        }
        
        
    }
    
    
    // MARK: - Collection View DataSource
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let item = self.gallery?.arrPhotos![indexPath.item]
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("itemGallery", forIndexPath: indexPath) as! GalleryCollectionViewCell
        
        
        cell.actualPageLabel.text = "\(indexPath.item + 1)"
        cell.totalPageLabel.text = "/ \((self.gallery?.arrPhotos!.count)!)"
        cell.textView.text = item?._description
        
        cell.imageView.hidden = true
        
        //Download de imagem
        if (item?.url != nil && item?.photoId != nil) {
            if let url = NSURL(string: item!.url!) {
                
                cell.imageView.sd_setImageWithURL(url, placeholderImage: nil, options: SDWebImageOptions.RefreshCached) {
                    img, error, _, _ in
                    
                    cell.imageView.hidden = false
                }
            }
        }
        
        return cell
    }

    
}
