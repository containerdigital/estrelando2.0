//
//  ProfileViewController.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/3/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ProfileCollectionCellDelegate, ASMediasFocusDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let daoCoreData = ContentDaoCoreData()
    let mediaFocusManager = ASMediaFocusManager()
    
    var content:Content?
    var tag = TagProfile()
    var age = String()
    var birth = String()
    var death = String()
    var imageURL: NSURL?
    
    var isSerie = false
    
    var cameFromDeepLink = false
    
    var cachePicture = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //NAVIGATION
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo-estrelando"))
        
        // Declare view top
        let header = UINib(nibName: "TopViewProfileCollectionViewCell", bundle: nil)
        self.collectionView?.registerNib(header, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerCell")
        
        let cell = UINib(nibName: "ProfilesCollectionViewCell", bundle: nil)
        self.collectionView?.registerNib(cell, forCellWithReuseIdentifier: "cell")
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.headerReferenceSize = CGSize(width: view.frame.size.width, height: 260)
        flowLayout.itemSize = CGSize(width: view.frame.size.width, height: 84)
        flowLayout.minimumLineSpacing = 5
        flowLayout.scrollDirection = UICollectionViewScrollDirection.Vertical
        flowLayout.sectionInset = UIEdgeInsetsMake(30, 0, 30, 0)
        
        self.collectionView?.collectionViewLayout = flowLayout
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        //API CALL
        let url = "http://app.estrelando.com.br/v1/tags/\(content!.id!)"

        let promise = EstrelandoAPI.tagInside(url)
        promise.then { () -> Void in
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.tag = promise.Result as! TagProfile
                if let birthday = self.tag.birthday {
                    self.age = "\(OtherFuncs().getAge(birthday))"
                    self.birth = self.tag.birthday!
                    self.death = (self.tag.death != nil) ? self.tag.death! : ""
                } else {
                    self.isSerie = true
                }
                
                self.collectionView?.reloadSections(NSIndexSet(index: 0))
            }
        }
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tag.arrContent!.count
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionElementKindSectionHeader) {
            if let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "headerCell", forIndexPath: indexPath) as? TopViewProfileCollectionViewCell {
                
                if isSerie {
                    header.imgView.contentMode = .ScaleAspectFit
                    header.date.hidden = true
                    header.traillingNameConstrant.constant = 95
                    header.traillingJobConstrant.constant = 95
                }
                
                header.imgView.image = content?.picture
                header.name.text  = content?.title?.string
                header.job.text   = tag.job?.uppercaseString
                header.birth.text = self.birth
                header.death.text = self.death
                header.age.text   = self.age
                header.date.text  = "\(header.age.text!) (\(header.birth.text!)\(header.death.text!))"
                
                //self.imageURL = NSURL(string: self.content!.foto1!)!
                self.mediaFocusManager.delegate = self
                self.mediaFocusManager.elasticAnimation = true
                self.mediaFocusManager.focusOnPinch = true
                
                self.mediaFocusManager.installOnView(header.imgView)
                
                return header
            }
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = self.collectionView?.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as? ProfilesCollectionViewCell {
            
            let item = self.tag.arrContent![indexPath.row]
            
            cell.delegate = self
            
            cell.setData(item, indexPath: indexPath)
            
            //Download de imagem
            if (item.foto1 != nil && item.id != nil) {
                if let url = NSURL(string: item.foto1!) {
                    cell.imgView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "default_image"), options: SDWebImageOptions.RefreshCached) {
                        img, error, _, _ in
                        
                        item.picture = img
                    }
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! ProfilesCollectionViewCell
        cell.contentView.backgroundColor = UIColor.lightGrayColor()
        cell.title.textColor = UIColor.whiteColor()
        cell.date.textColor = UIColor.whiteColor()
    }
    
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! ProfilesCollectionViewCell
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.title.textColor = UIColor.lightGrayColor()
        cell.date.textColor = UIColor.lightGrayColor()
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath)
        cell?.highlighted = true
        
        let item = self.tag.arrContent![indexPath.row]
        
        switch item.type! {
        case "news":
            if let tela = self.storyboard?.instantiateViewControllerWithIdentifier("internaNotas") as? InternaViewController {
                tela.content = item
                tela.stringCategory = OtherFuncs().translateProfileType(self.tag.profileType!)
                
                self.navigationController?.pushViewController(tela, animated: true)
            }
            
            break
        case "gallery":
            if let tela = self.storyboard?.instantiateViewControllerWithIdentifier("galleryViewController") as? GalleryViewController {
                tela.content = item
                self.navigationController?.pushViewController(tela, animated: true)
            }
            
            break
        case "ranking":
            if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("rankingViewController") as? RankingViewController {
                vc.content = item
                vc.categorie = OtherFuncs().translateProfileType(self.tag.profileType!)
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            break
        default:
            break
        }
    }
    
    
    // MARK: - IBActions
    
    @IBAction func rightButtonAction(sender: AnyObject) {
        //call searchViewController
        if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("shareViewController") as? SearchViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func dismissView(sender: AnyObject) {
        guard cameFromDeepLink else {
            self.navigationController?.popViewControllerAnimated(true); return
        }
        
        let delegate = UIApplication.sharedApplication().delegate
        let revealController = delegate?.window!!.rootViewController as! SWRevealViewController
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = story.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        
        let navController = UINavigationController(rootViewController: vc)
        navController.setViewControllers([vc], animated: true)
        
        revealController.pushFrontViewController(navController, animated: true)
    }
    
    //Menu footer 
    
    @IBAction func leftFooterButton(sender: AnyObject) {
        if let textToShare = self.tag.link {
            let objectsToShare : Array = [textToShare];
            
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - DELEGATES
    
    //Salva a matéria selecionada localmente ou deleta ela se já estiver salva.
    func saveOrDelete(content: Content, indexPath: NSIndexPath) {
        if let result = daoCoreData.select(content.id!.stringValue) as? [ContentDto] {
            if result.count > 0 {
                let alert = UIAlertController(title: "", message: "A matéria será apagada do seu celular, você tem certeza?", preferredStyle: .Alert)
                
                alert.addAction(UIAlertAction(title: "Não", style: .Cancel, handler: nil))
                
                let deleteAction = UIAlertAction(title: "Sim", style: .Default, handler: { Void in
                    if self.daoCoreData.delete(result[0]) {
                        let cell = self.collectionView.cellForItemAtIndexPath(indexPath) as! ProfilesCollectionViewCell
                        cell.readLatter.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
                    }
                })
                
                alert.addAction(deleteAction)
                
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                content.fullContent().then {
                    let entity = self.daoCoreData.entityObject() as! ContentDto
                    
                    entity.id = content.id!.intValue
                    entity.categories = content.categories
                    entity.image = UIImagePNGRepresentation(content.picture!)
                    entity.postDate = content.postDate
                    entity.summary = content.summary
                    entity.type = content.type
                    entity.contentType = content.contentType
                    entity.link = content.link
                    entity.title = content.title
                    entity.url = content.foto1
                    entity.postText = content.postText
                    entity.fullText = content.fullText
                    
                    if self.daoCoreData.insert(entity) {
                        let cell = self.collectionView.cellForItemAtIndexPath(indexPath) as! ProfilesCollectionViewCell
                        cell.readLatter.setImage(UIImage(named: "bt_read_latter_focused"), forState: .Normal)
                    }
                }
            }
        }
    }
    
    
    // MARK: - MEDIA FOCUS MANAGER DELEGATES
    
    //
    func parentViewControllerForMediaFocusManager(mediaFocusManager: ASMediaFocusManager!) -> UIViewController! {
        return self
    }
    
    //
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, mediaURLForView view: UIView!) -> NSURL! {
        return self.imageURL
    }
    
    //
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, titleForView view: UIView!) -> String! {
        return self.content?.title?.string
    }
    
}
