//
//  HomeCollectionViewCell.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewCell: UIImageView!
    @IBOutlet weak var labelMateria: UILabel!
    @IBOutlet weak var labelCategoria: UILabel!
//    @IBOutlet weak var viewCategoria: UIView!
    
}
