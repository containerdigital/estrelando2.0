//
//  Content+CoreDataProperties.swift
//  Estrelando
//
//  Created by Renato Mendes on 5/24/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

@objc(ContentDto)
class ContentDto : NSManagedObject {
    @NSManaged var id: Int32
    @NSManaged var categories: String?
    @NSManaged var url: String?
    @NSManaged var image: NSData?
    @NSManaged var link: String?
    @NSManaged var postDate: String?
    @NSManaged var summary: String?
    @NSManaged var title: NSAttributedString?
    @NSManaged var postText: NSAttributedString?
    @NSManaged var fullText: NSAttributedString?
    @NSManaged var type: String?
    @NSManaged var contentType: String?
}
