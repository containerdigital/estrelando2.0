//
//  InternaViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 15/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class InternaViewController: UIViewController, UIScrollViewDelegate, UITextViewDelegate, ASMediasFocusDelegate {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgNotice: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var leftButtonFooter: UIButton!
    @IBOutlet weak var centerButtonFooter: UIButton!
    @IBOutlet weak var rightButtonFooter: UIButton!
    
    @IBOutlet weak var labelViewCategory: UILabel!
    
    let mediaFocusManager = ASMediaFocusManager()
    let daoCoreData = ContentDaoCoreData()
    
    var stringCategory:String?
    var imageURL: NSURL?
    
    var viewShare : UIView = UIView()
    var viewPictureToZoom : UIView = UIView()
    var imgPict : UIImageView = UIImageView()
    var imgX : UIImageView = UIImageView()
    var blurView : UIVisualEffectView = UIVisualEffectView()
    
    var content:Content?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NAVIGATION
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo-estrelando"))
        
        //
        self.initElementGraphics()
        self.configureFooterButtons()
        self.textView.delegate = self
        self.scrollView.delegate = self
        self.styleViewAndLabelColor(self.stringCategory!)
        
        self.imageURL = NSURL(string: self.content!.foto1!)!
        self.mediaFocusManager.delegate = self
        self.mediaFocusManager.elasticAnimation = true
        self.mediaFocusManager.focusOnPinch = true
        
        self.mediaFocusManager.installOnView(self.imgNotice)
    }
    
    func initElementGraphics() {
        if let pict = content!.picture {
            self.imgNotice.image = pict
            let viewImg = UIView(frame: CGRectMake(20, 2.5, 40, 40))
            let imgIMG = UIImageView(frame: CGRectMake(0, 1, 40, 40))
            let tapZoomPicture = UITapGestureRecognizer(target: self, action: #selector(InternaViewController.ZoomPict(_:)))
            
            self.imgNotice.userInteractionEnabled = true
            self.imgNotice.addGestureRecognizer(tapZoomPicture)
            
            imgIMG.image = pict
            viewImg.addSubview(imgIMG)
            viewImg.backgroundColor = UIColor.whiteColor()
            viewImg.layer.masksToBounds = true
            viewImg.layer.cornerRadius = 20
            viewImg.layer.borderColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7).CGColor
            viewImg.layer.borderWidth = 0.5
            _ = UIBarButtonItem(customView: viewImg)
        }
        
        self.labelTitle.numberOfLines = 4
        if let _ = content?.title {
            self.labelTitle.text = content!.title!.string.uppercaseString
        }
    }
    
    func configureFooterButtons() {
        self.leftButtonFooter.setImage(UIImage(named: "bt_share"), forState: .Normal)
//        self.rightButtonFooter.setImage(UIImage(named: "bt_comments"), forState: .Normal)
        
        //VERIFICAÇÃO DE CORE DATA
        if let result = daoCoreData.select(content!.id!.stringValue) as? [ContentDto] {
            if result.count > 0 {
                self.centerButtonFooter.setImage(UIImage(named: "bt_read_latter_focused"), forState: .Normal)
                self.date.text = result[0].postDate
                self.textView.attributedText = result[0].fullText
                
                content?.fullText = String.converString(result[0].fullText!.string)
                content?.link = result[0].link
                
                self.resizeScrollView()
            } else {
                self.centerButtonFooter.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
                reloadAndCreatePage()
            }
        }
    }
    
    func reloadAndCreatePage(){
        if let content = content{
            content.fullContent().then({ () -> Void in
                self.date.text = content.postDate
                self.textView.attributedText = content.fullText
                self.resizeScrollView()
            })
        }
    }
    
    func ZoomPict(sender:UIImageView){
        let tapCloseZoomPicture = UITapGestureRecognizer(target: self, action: #selector(InternaViewController.tapCloseZoomPicture(_:)))
        let labelX = UILabel(frame: CGRectMake(self.imgX.frame.size.width/3.5, 0, self.imgX.frame.size.width, self.imgX.frame.size.height))
        labelX.textColor = UIColor.whiteColor()
        labelX.text = "X"
        self.imgX = UIImageView(frame: CGRectMake(self.viewPictureToZoom.frame.size.width - 60 , self.viewPictureToZoom.frame.size.height - 75, 30, 30))
        self.imgX.layer.masksToBounds = true
        self.imgX.layer.cornerRadius = 15.0
        self.imgX.backgroundColor = UIColor(red: 0.686, green: 0.0, blue: 0.125, alpha: 0.5)
        self.imgX.addSubview(labelX)
        self.viewPictureToZoom = UIView(frame: CGRectMake(0, self.view.frame.size.height / 3.5, self.view.frame.size.width, 200))
        self.imgPict = UIImageView(frame: CGRectMake(0, 0, viewPictureToZoom.frame.size.width, viewPictureToZoom.frame.size.height))
        self.imgPict.image = self.content?.picture
        self.imgPict.contentMode = .ScaleAspectFit
        self.viewPictureToZoom.addSubview(imgPict)

        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        self.blurView = UIVisualEffectView(effect: darkBlur)
        self.blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        labelX.userInteractionEnabled = true
        labelX.addGestureRecognizer(tapCloseZoomPicture)
        self.blurView.userInteractionEnabled = true
        self.blurView.addGestureRecognizer(tapCloseZoomPicture)
        self.imgPict.userInteractionEnabled = true
        self.imgPict.addGestureRecognizer(tapCloseZoomPicture)
        self.imgX.userInteractionEnabled = true
        self.imgX.addGestureRecognizer(tapCloseZoomPicture)
        self.viewPictureToZoom.userInteractionEnabled = true
        self.viewPictureToZoom.addGestureRecognizer(tapCloseZoomPicture)
        self.blurView.addSubview(viewPictureToZoom)
        self.blurView.addSubview(self.imgX)
    }

    func tapCloseZoomPicture(sender:AnyObject){
        self.imgPict.removeFromSuperview()
        self.viewPictureToZoom.removeFromSuperview()
        self.blurView.removeFromSuperview()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        UIView.animateWithDuration(1.0, delay: 0.5, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.viewFooter.hidden = true
            
            }, completion: { (Bool) -> Void in
        
        })
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.viewFooter.hidden = false
    }
    
    
    func resizeScrollView(){
        self.textView.scrollEnabled = false
        var size:CGFloat = self.labelTitle.frame.height
        size = size + self.imgNotice.frame.height
        size = size + self.textView.getContentSize().height
        size = size + 100

        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: size)
        self.textView.sizeToFit()
        self.textView.layoutIfNeeded()
        self.view.updateConstraints()
    }
    
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        let string = "\(URL.standardizedURL!)"
        
        self.content = Content(link: string)
        self.reloadAndCreatePage()
        
        return false
    }
    
    func styleViewAndLabelColor(category:String){
        switch(category){
            
        case "estilo":
            self.labelTitle.textColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
            self.labelViewCategory.text = "   \(category.uppercaseString)"
            self.labelViewCategory.backgroundColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
            self.textView.tintColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)

            break
            
        case "realities":
            self.labelViewCategory.text = "   \(category.uppercaseString)"
            self.labelTitle.textColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            self.labelViewCategory.backgroundColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            self.textView.tintColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            
            break
            
        case "séries":
            self.labelViewCategory.text = "   \(category.uppercaseString)"
            self.labelTitle.textColor = String.colorForCategory(category)
            self.labelViewCategory.backgroundColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)
            self.textView.tintColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)

            break
            
        case "teen":
            self.labelViewCategory.text = "   \(category.uppercaseString)"
            self.labelTitle.textColor = String.colorForCategory(category)
            self.labelViewCategory.backgroundColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            self.textView.tintColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            
            break
            
        default:
            self.labelViewCategory.text = "   \(category.uppercaseString)"
            self.textView.tintColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            self.labelTitle.textColor = String.colorForCategory(category)
            self.labelViewCategory.backgroundColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            
            break
            
        }
    }
    
    //Ação de compartilhar, permite compartilhar nas redes sociais e mais
    @IBAction func leftButtonAction(sender: AnyObject) {
        if let textToShare = self.content!.link {
            let objectsToShare : Array = [textToShare];
            
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }
    
    //Ação de ler depois, salva matéria no coredata e permite excluir caso já tenha sido salva
    @IBAction func centerButtonAction(sender: AnyObject) {
        if let result = daoCoreData.select(content!.id!.stringValue) as? [ContentDto] {
            if result.count > 0 {
                let alert = UIAlertController(title: "", message: "A matéria será apagada do seu celular, você tem certeza?", preferredStyle: .Alert)
                
                alert.addAction(UIAlertAction(title: "Não", style: .Cancel, handler: nil))
                
                let deleteAction = UIAlertAction(title: "Sim", style: .Default, handler: { Void in
                    if self.daoCoreData.delete(result[0]) {
                        self.centerButtonFooter.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
                    }
                })
                
                alert.addAction(deleteAction)
                
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                let entity = daoCoreData.entityObject() as! ContentDto
                
                entity.id = content!.id!.intValue
                entity.categories = content?.categories
                entity.image = UIImagePNGRepresentation(content!.picture!)
                entity.postDate = content?.postDate
                entity.summary = content?.summary
                entity.type = content?.type
                entity.contentType = content?.contentType
                entity.link = content?.link
                entity.title = content?.title
                entity.url = content?.foto1
                entity.postText = content?.postText
                entity.fullText = content?.fullText
                
                if daoCoreData.insert(entity) {
                    self.centerButtonFooter.setImage(UIImage(named: "bt_read_latter_focused"), forState: .Normal)
                }
            }
        }
    }
    
    @IBAction func rightButtonAction(sender: AnyObject) {
        //call searchViewController
        if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("shareViewController") as? SearchViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
//    @IBAction func rightFooterButtonAction(sender: AnyObject) {
//        //ação para chamar view de comentários.
//        if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("navigationCommentsViewController") as? UINavigationController {
//            
//            if let comments = vc.topViewController as? CommentsViewController{
//                comments.content = self.content
//                self.presentViewController(vc, animated: true, completion: nil)
//                
//            }
//        }
//    }
    
    @IBAction func dismissView(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    // MARK: - MEDIA FOCUS MANAGER DELEGATES
    
    //
    func parentViewControllerForMediaFocusManager(mediaFocusManager: ASMediaFocusManager!) -> UIViewController! {
        return self
    }
    
    //
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, mediaURLForView view: UIView!) -> NSURL! {
        return self.imageURL
    }
    
    //
    func mediaFocusManager(mediaFocusManager: ASMediaFocusManager!, titleForView view: UIView!) -> String! {
        return self.labelTitle.text
    }
    
}
