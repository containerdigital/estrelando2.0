//
//  Gallery.swift
//  Estrelando
//
//  Created by iOS Developer on 28/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class GalleryModel: Content {
    
    var intro : NSMutableAttributedString?
    var slug : String?
    var _type : String?
    var _link : String?
    var writer : String?
    var writerEmail : String?
    var titleSEO : NSMutableAttributedString?
    var keywordsSEO : String?
    var descriptionSEO : String?
    var status : String?
    var quizType : String?
    var publishedAt : String?
    var createdAt : String?
    var updatedAt : String?
    var deletedAt : String?
    var SiteId : NSNumber?
    var votos1 : NSNumber?
    var votos2 : NSNumber?
    var votos3 : NSNumber?
    var votos4 : NSNumber?
    var votos5 : NSNumber?
    var showHome : NSNumber?
    var arrPhotos : [PhotoModel]?
    var arrTags : NSMutableArray?
    
    override init(dictionary: NSDictionary) {
        super.init()
        
        arrPhotos = [PhotoModel]()
        arrTags = NSMutableArray()
        
        self.initAllKeys(dictionary)
    }
    
    override func initAllKeys(dictionary: NSDictionary) {
        
        if let id = dictionary["id"] as? NSNumber {
            super.id = id
        }
        
        if let title = dictionary["title"] as? String {
            super.title = String.converString(title)
        }
        
        if let summary = dictionary["summary"] as? String {
            super.summary = summary
        }
        
        if let fullText = dictionary["fullText"] as? String {
            super.postText = String.converString(fullText)
        }
        
        if let intro = dictionary["intro"] as? String {
            self.intro = String.converString(intro)
        }
        
        if let slug = dictionary["slug"] as? String {
            self.slug = slug
        }
        
        if let _type = dictionary["type"] as? String {
            self._type = _type
        }
        
        if let _link = dictionary["link"] as? String {
            self._link = _link
        }
        
        if let writer = dictionary["writer"] as? String {
            self.writer = writer
        }
        
        if let writerEmail = dictionary["writerEmail"] as? String {
            self.writerEmail = writerEmail
        }
        
        if let titleSEO = dictionary["titleSEO"] as? String {
            self.titleSEO = String.converString(titleSEO)
        }
        
        if let keywordsSEO = dictionary["keywordsSEO"] as? String {
            self.keywordsSEO = keywordsSEO
        }
        
        if let descriptionSEO = dictionary["descriptionSEO"] as? String {
            self.descriptionSEO = descriptionSEO
        }
        
        if let status = dictionary["status"] as? String {
            self.status = status
        }
        
        if let quizType = dictionary["quizType"] as? String {
            self.quizType = quizType
        }
        
        if let publishedAt = dictionary["publishedAt"] as? String {
            self.publishedAt = publishedAt
        }
        
        if let createdAt = dictionary["createdAt"] as? String {
            self.createdAt = createdAt
        }
        
        if let updatedAt = dictionary["updatedAt"] as? String {
            self.updatedAt = updatedAt
        }
        
        if let deletedAt = dictionary["deletedAt"] as? String {
            self.deletedAt = deletedAt
        }
        
        if let SiteId = dictionary["SiteId"] as? NSNumber {
            self.SiteId = SiteId
        }
        
        if let votos1 = dictionary["votos1"] as? NSNumber {
            self.votos1 = votos1
        }
        
        if let votos2 = dictionary["votos2"] as? NSNumber {
            self.votos2 = votos2
        }
        
        if let votos3 = dictionary["votos3"] as? NSNumber {
            self.votos3 = votos3
        }
        
        if let votos4 = dictionary["votos4"] as? NSNumber {
            self.votos4 = votos4
        }
        
        if let votos5 = dictionary["votos5"] as? NSNumber {
            self.votos5 = votos5
        }
        
        if let showHome = dictionary["showHome"] as? NSNumber {
            self.showHome = showHome
        }
        
        if let arr = dictionary["Files"] as? NSArray {
            for item in arr {
                if item["type"] as! String == "GALLERY" {
                    let photo: PhotoModel = PhotoModel(dictionary: item as! NSDictionary)
                    self.arrPhotos?.append(photo)
                }
            }
        }
        
        if let arr = dictionary["Tags"] as? NSArray {
            for item in arr {
                self.arrTags?.addObject(item)
            }
        }
    }
    
}
