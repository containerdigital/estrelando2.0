//
//  ListagensViewController.swift
//  Estrelando
//
//  Created by iOS Developer on 13/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit
import SDWebImage

class ListagensViewController: BaseRootViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var labelCat: UILabel!
    
    var categorie : String!
    var type : String!
    var profileType : String!
    
    var array:Array<Content> = Array<Content>()
    var currentPage:Int = 0
    
    let cachePicture = NSMutableDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "LoadingCell")
    }

    func loadCategorie(){
        let promise : Promise
        
        var url = ""
        
        if type == "profiles" {
            url = "http://app.estrelando.com.br/v1/tags?taxonomy=ART_PROFILE&profileType=\(profileType)&page=\(currentPage)&perPage=\(25)"
            print(url)
            
            promise = EstrelandoAPI.tagsHelper(url)
        } else {
            if categorie == "séries" {
                url = "http://app.estrelando.com.br/v1/series/\(type)?page=\(currentPage)&perPage=\(25)"
            } else {
                url = "http://app.estrelando.com.br/v1/\(categorie)/\(type)?page=\(currentPage)&perPage=\(25)"
            }
            promise = EstrelandoAPI.contentHelper(url)
        }
        
        promise.then { () -> Void in
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.array.appendContentsOf(promise.Result as! Array<Content>)
                self.tableView?.reloadData()
            }
            
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array.count + 1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 70))
        let lbl = ColorForCategorie().textColorForCategorie(categorie)
        
        let label = UILabel(frame: CGRectMake(20, 0, self.view.frame.width, 70))
        label.text = categorie.uppercaseString
        label.font = UIFont(name: "Roboto-Medium", size: 25.0)
        label.textColor = lbl.textColor

        vw.backgroundColor = UIColor.whiteColor()
        vw.addSubview(label)
        
        return vw
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.item < array.count {
            return rowCellForIndexPath(indexPath)
        } else {
            fetchMoreItems()
            return loadingCellForIndexPath(indexPath)
        }
    }
    
    func rowCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell {

        let cell:Listagens2TableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! Listagens2TableViewCell
        
        cell.imageNot?.hidden = false
        cell.setEditing(false, animated: true)
        
        let item = self.array[indexPath.row]
        
        cell.labelTitle.attributedText = item.title
        cell.labelTitle.numberOfLines = 4
        cell.imageNot?.image = item.picture
        cell.imgConstH?.constant = 60.0
        cell.imgConstW?.constant = 60.0
        cell.updateConstraintsIfNeeded()
        
        //Download de imagem
        if (item.foto1 != nil && item.id != nil) {
            if let url = NSURL(string: item.foto1!) {
                cell.imageNot!.sd_setImageWithURL(url, placeholderImage: UIImage(named: "default_image"), options: SDWebImageOptions.RefreshCached) {
                    img, error, _, _ in
                    
                    item.picture = img
                }
            }
        }
        
        return cell
    }
    
    //Loading da view
    func loadingCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LoadingCell", forIndexPath: indexPath)
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.center = cell.center
        
        cell.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        return cell
    }
    
    //Busca novos elementos
    func fetchMoreItems() {
        currentPage = currentPage + 1 // Chama próxima página
        loadCategorie()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch(type){
        
        case "news":
            self.performSegueWithIdentifier("internDestination", sender: nil)
            break
            
        case "gallery":
            self.performSegueWithIdentifier("galleryDestination", sender: nil)
            break
            
        case "ranking":
            self.performSegueWithIdentifier("rankingDestination", sender: nil)
            
        case "profiles":
            self.performSegueWithIdentifier("profileDestination", sender: nil)
            break
            
        default:
            break
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexPath : NSIndexPath = self.tableView.indexPathForSelectedRow!
        
        let item = self.array[indexPath.row]
        
        let identifier = segue.identifier!
        switch(identifier){
            
        case "internDestination":
            let tela = segue.destinationViewController as? InternaViewController
            tela?.content = item
            tela?.stringCategory = categorie
            
            break
        case "galleryDestination":
            let tela = segue.destinationViewController as? GalleryViewController
            tela?.content = item
            
            break
        case "rankingDestination":
            let tela = segue.destinationViewController as? RankingViewController
            tela?.content = item
            tela?.categorie = categorie
            
            break
        case "profileDestination":
            let tela = segue.destinationViewController as? ProfileViewController
            tela?.content = item
            
            break
        default:
            break
        }
    }
    
}
