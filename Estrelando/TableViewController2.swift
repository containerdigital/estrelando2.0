//
//  TableViewController2.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class TableViewController2: UITableViewController {

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TableViewController2.disableTouch), name: "disableTouch", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TableViewController2.enableTouch), name: "enableTouch", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TableViewController2.enableTouch), name: "keyboardShow", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TableViewController2.disableTouch), name: "keyboardHide", object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver("closeMenu")
        NSNotificationCenter.defaultCenter().removeObserver("abrirMenu")
    }
    
    func disableTouch(){
        self.view.userInteractionEnabled = false
    }
    
    func enableTouch(){
        self.view.userInteractionEnabled = true
    }
    
    func disableKeyboard() {
        self.view.endEditing(true)
    }
    
    func enableKeyboard() {
        self.view.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
