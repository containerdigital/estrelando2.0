//
//  Promise.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit


protocol PromiseDelegate:NSObjectProtocol{
    func then(promise:Promise)
}

enum PromiseStatus:Int{

    case running = 1
    case error = -1
    case completed = 2
}

class Promise: NSObject {

    var Result:AnyObject?
    var status:PromiseStatus = PromiseStatus.running
    var callBacks:Dictionary<String,Array<()->Void>> = Dictionary<String,Array<()->Void>>()
    
    func then(closure:()->Void)->Promise{
        if((self.callBacks["success"]) == nil){
        self.callBacks["success"] = Array<()->Void>()
        }
        (self.callBacks["success"])?.append(closure)
        return self
        
    }
    
    func recover(closure:()->Void)->Promise{
        if((self.callBacks["error"]) == nil){
            self.callBacks["error"] = Array<()->Void>()
        }
        (self.callBacks["error"])?.append(closure)
        return self
    }
    
    func resolv(result:AnyObject){
        self.Result = result
        self.status = PromiseStatus.completed
        if let callbacks = self.callBacks["success"]{
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            for i in 0..<callbacks.count {
                let callback = callbacks[i]
                callback()
            }
            
        }
        
    }
    
    func rejects(result:AnyObject){
        self.Result = result
        self.status = PromiseStatus.error
        if let callbacks = self.callBacks["error"]{
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            for i in 0..<callbacks.count {
                let callback = callbacks[i]
                    callback()
            }

        }
        
    }
    
    override init() {
        super.init()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
}

