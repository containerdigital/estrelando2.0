//
//  GalleryCollectionViewCell.swift
//  Estrelando
//
//  Created by Thiago Harada on 6/9/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var actualPageLabel: UILabel!
    @IBOutlet weak var totalPageLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func awakeFromNib() {
        
    }
    
}
