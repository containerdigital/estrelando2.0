//
//  CommentsTableViewCell.swift
//  Estrelando
//
//  Created by Thiago Harada on 6/13/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profilePictureImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var curtirButton: UIButton!
    @IBOutlet weak var responderButton: UIButton!
    @IBOutlet weak var marcarSpamButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
