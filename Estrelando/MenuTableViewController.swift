//
//  TableViewController.swift
//  testeMenuEstrelando
//
//  Created by Renato Mendes on 6/1/16.
//  Copyright © 2016 Renato Mendes. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    let options = ["HOME"]
    let editorial = SwiftAccordionCells()
    let others = ["LER DEPOIS"]
    
    let optionImages = [UIImage(named: "bt_home")]
    let othersImages = [UIImage(named: "bt_ler_depois")]
    
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    
    var currentHeader : String?
    var selectedItem : String?
    var isExpand : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let accordingNIB = UINib(nibName: "AccordingCellsMenuTableViewCell", bundle: nil)
        self.tableView.registerNib(accordingNIB, forCellReuseIdentifier: "accordingCell")
        
        self.setup()
    }
    
    func setup() {
        self.editorial.append(SwiftAccordionCells.HeaderItem(value: "CELEBRIDADES"))
        self.editorial.append(SwiftAccordionCells.Item(value: "NOTÍCIAS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "FOTOS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "RANKING"))
        self.editorial.append(SwiftAccordionCells.Item(value: "PERFIS"))
        
        self.editorial.append(SwiftAccordionCells.HeaderItem(value: "ESTILO"))
        self.editorial.append(SwiftAccordionCells.Item(value: "NOTÍCIAS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "FOTOS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "RANKING"))
        
        self.editorial.append(SwiftAccordionCells.HeaderItem(value: "REALITIES"))
        self.editorial.append(SwiftAccordionCells.Item(value: "NOTÍCIAS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "FOTOS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "RANKING"))
        
        self.editorial.append(SwiftAccordionCells.HeaderItem(value: "SÉRIES"))
        self.editorial.append(SwiftAccordionCells.Item(value: "NOTÍCIAS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "FOTOS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "RANKING"))
        self.editorial.append(SwiftAccordionCells.Item(value: "PERFIS"))
        
        self.editorial.append(SwiftAccordionCells.HeaderItem(value: "TEEN"))
        self.editorial.append(SwiftAccordionCells.Item(value: "NOTÍCIAS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "FOTOS"))
        self.editorial.append(SwiftAccordionCells.Item(value: "RANKING"))
        self.editorial.append(SwiftAccordionCells.Item(value: "PERFIS"))
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.options.count
        case 1:
            return self.editorial.items.count
        case 2:
            return self.others.count
        default:
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let item = self.editorial.items[indexPath.row]
        if indexPath.section == 1 {
            if item is SwiftAccordionCells.HeaderItem {
                return 65
            } else if (item.isHidden) {
                return 0
            } else {
                return 44
            }
        } else {
            return 65
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return ""
        case 1:
            return "EDITORIAL"
        case 2:
            return " "
        default:
            return ""
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if section == 1 {
            let header = view as! UITableViewHeaderFooterView
            header.textLabel?.textColor = UIColor.grayColor()
            header.textLabel?.font = UIFont(name: "Roboto-Medium", size: 16)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! DefaultMenuTableViewCell
            
            let item = self.options[indexPath.row]
            cell._title.text = item
            cell._icon.image = self.optionImages[indexPath.row]
            cell._separator.hidden = true
            
            return cell
            
        case 1:
            let item = self.editorial.items[indexPath.row]
            
            if item is SwiftAccordionCells.HeaderItem {
                let cell = tableView.dequeueReusableCellWithIdentifier("accordingCell", forIndexPath: indexPath) as! AccordingCellsMenuTableViewCell
                let category = item.value as? String
                cell._title.text = category
                cell._palette.backgroundColor = cell.colorForCategory(category!)
                
                self.currentHeader = category
                
                return cell
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("accordingDetailCell", forIndexPath: indexPath) as! DefaultMenuTableViewCell
            cell._title.text = item.value as? String
            cell.backgroundColor = cell.colorForCategory(self.currentHeader!)
            
            if (indexPath.row == self.editorial.items.count - 1) { cell._separator.hidden = true }
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! DefaultMenuTableViewCell
            
            let item = self.others[indexPath.row]
            cell._title.text = item
            cell._icon.image = self.othersImages[indexPath.row]
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            self.performSegueWithIdentifier("homeSegue", sender: nil)
            
        case 1:
            let item = self.editorial.items[indexPath.row]
            
            if item is SwiftAccordionCells.HeaderItem {
                
                self.tableView.beginUpdates()
                
                currentHeader = item.value as? String
                
                if self.selectedHeaderIndex == nil {
                    self.selectedHeaderIndex = indexPath.row
                } else {
                    self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                    self.selectedHeaderIndex = indexPath.row
                }
                
                if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                    let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: previouslySelectedHeaderIndex, inSection: 1)) as! AccordingCellsMenuTableViewCell
                    cell._icon.image = UIImage(named: "right_arrow")
                    self.editorial.collapse(previouslySelectedHeaderIndex)
                }
                
                if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                    let cell = tableView.cellForRowAtIndexPath(indexPath) as! AccordingCellsMenuTableViewCell
                    cell._icon.image = UIImage(named: "botton_arrow")
                    self.editorial.expand(self.selectedHeaderIndex!)
                } else {
                    self.selectedHeaderIndex = nil
                    self.previouslySelectedHeaderIndex = nil
                }
                
                self.tableView.endUpdates()
                
            } else {
                self.performSegueWithIdentifier("listSegue", sender: nil)
            }
            
        case 2:
            self.performSegueWithIdentifier("readLatterSegue", sender: nil)
        default:
            break
        }
    }
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexPath = tableView.indexPathForSelectedRow
        
        switch indexPath!.section {
        case 1:
            if let header = currentHeader {
                selectedItem = self.editorial.items[indexPath!.row].value as? String
                
                let nav = segue.destinationViewController as? UINavigationController
                let vc  = nav?.viewControllers[0] as? ListagensViewController
                vc?.categorie = header.lowercaseString
                
                if let item = selectedItem {
                    switch item.lowercaseString {
                    case"notícias":
                        vc?.type = "news"
                        break
                    case"fotos":
                        vc?.type = "gallery"
                        break
                    case "ranking":
                        vc?.type = "ranking"
                        break
                    case "perfis":
                        vc?.type = "profiles"
                        vc?.profileType = OtherFuncs().getProfileTypes(header.lowercaseString)
                        break
                    case "quiz":
                        vc?.type = "quiz"
                    default:
                        break
                    }
                }
            }
        
        default:
            break
        }
    }
    
}
