//
//  PhotoModel.swift
//  Estrelando
//
//  Created by Thiago Harada on 5/4/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class PhotoModel: NSObject {
    
    var type: String?
    var title: String?
    var _description: String?
    var credits: String?
    var order: NSNumber?
    var url: String?
    var photoId: NSNumber?
    init(dictionary: NSDictionary) {
        super.init()
        self.initAllKeys(dictionary)
    }
    
    func initAllKeys(dictionary: NSDictionary) {
        
        if let type = dictionary["type"] as? String {
            self.type = type
        }
        
        if let title = dictionary["title"] as? String {
            self.title = String.converString(title)?.string
        }
        
        if let description = dictionary["description"] as? String {
            self._description = String.converString(description)?.string
        }
        
        if let credits = dictionary["credits"] as? String {

            self.credits = credits
        }
        
        if let order = dictionary["order"] as? NSNumber {
            self.order = order
        }
        
        if let dict = dictionary["Files"] as? NSDictionary {
            self.url = dict["url"] as? String
            //print("URLDICT",  dict["url"])
            self.photoId = dict["id"] as? NSNumber
        }

                
    }
    
}
