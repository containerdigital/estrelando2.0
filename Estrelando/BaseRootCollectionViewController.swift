//
//  BaseRootCollectionViewController.swift
//  Estrelando
//
//  Created by Renato Mendes on 3/15/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class BaseRootCollectionViewController: UICollectionViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set left bar button iten , on nav controller.
        let hamburguerItem = UIImage(named: "bt_menu")
        let menuButton = UIBarButtonItem(image: hamburguerItem, style: UIBarButtonItemStyle.Plain, target: self, action: nil)
        self.navigationItem.leftBarButtonItem = menuButton
        
       // self.navigationItem.leftBarButtonItem?.tintColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
        //Set Logo 'Estrelando' on nav controller.
        let logo = UIImage(named: "logo-estrelando")
        let imageView = UIImageView(image: logo)
        self.navigationController?.navigationItem.titleView = imageView
        self.navigationItem.titleView = imageView
        
        let searchItem = UIImage(named: "bt_search")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: searchItem, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(BaseRootCollectionViewController.callSearch))
        
        //Menu configs
        if self.revealViewController() != nil {
            self.revealViewController().rearViewRevealWidth = self.view.bounds.width - 60
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func callSearch() {
        if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("shareViewController") as? SearchViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
