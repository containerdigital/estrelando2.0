//
//  OtherFuncs.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/6/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import Foundation

class OtherFuncs {

    // Retorna o profileType do menu atual
    func getProfileTypes(container: String) -> String {
        var profileType = ""
        
        switch container.lowercaseString {
        case "celebridades":
            profileType = "CELEBRITIES"
        case "realities":
            profileType = "REALITY"
        case "séries":
            profileType = "SERIE"
        case "teen":
            profileType = "TEEN"
        default:
            profileType = ""
        }
        
        return profileType
    }
    
    // Retorna o profileType em pt/br
    func translateProfileType(profile: String) -> String {
        switch profile.uppercaseString {
        case "CELEBRITIES":
            return "CELEBRIDADES"
        case "REALITY":
            return "REALITIES"
        case "SERIE":
            return "SÉRIES"
        case "TEEN":
            return "TEEN"
        default:
            return ""
        }
    }

    // Retorna uma determinada idade
    func getAge(birth: String) -> Int {
        let df = NSDateFormatter()
        df.dateFormat = "dd/MM/yyyy"
        
        let dtBirth = df.dateFromString(birth)
        
        let ageComponents = NSCalendar.currentCalendar().components(.Year,
                                                fromDate: dtBirth!,
                                                toDate: NSDate(),
                                                options: NSCalendarOptions.MatchFirst)
        
        return ageComponents.year
    }
    
}