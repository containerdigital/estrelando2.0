//
//  TopViewProfileCollectionViewCell.swift
//  Estrelando
//
//  Created by Renato Mendes on 5/5/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class TopViewProfileCollectionViewCell: UICollectionReusableView {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var job: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var death: UILabel!
    @IBOutlet weak var birth: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var traillingNameConstrant: NSLayoutConstraint!
    @IBOutlet weak var traillingJobConstrant: NSLayoutConstraint!

    override func awakeFromNib() {

    }

}

