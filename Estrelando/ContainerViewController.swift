//
//  ViewController.swift
//  Estrelando
//
//  Created by Web1 on 14/09/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    var content:UINavigationController?;
    
    func loadPage(view:UIViewController) {
            self.content?.pushViewController(view, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Instantiate Views
//        self.navigationController?.hidesBarsOnSwipe = true
        

        self.content = self.storyboard!.instantiateViewControllerWithIdentifier("Content") as? UINavigationController;
        
        // Adjust Positions

        self.content?.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.size.height);
        
        
        // Add to stage
        self.view.addSubview(self.content!.view);
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

