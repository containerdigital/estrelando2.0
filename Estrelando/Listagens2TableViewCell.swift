//
//  Listagens2TableViewCell.swift
//  Estrelando
//
//  Created by iOS Developer on 13/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class Listagens2TableViewCell: UITableViewCell {

    @IBOutlet weak var view:UIView?
    @IBOutlet weak var imgConstH: NSLayoutConstraint?
    @IBOutlet weak var imgConstW: NSLayoutConstraint?
    @IBOutlet weak var imageNot: UIImageView?
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewColorCategory: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
       // super.setSelected(selected, animated: animated)
    // Configure the view for the selected state
    }

    
    func styleCategory(category:String){
        switch(category){
        
        
            
        case "estilo":
            self.labelTitle.textColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
            self.viewColorCategory.backgroundColor = UIColor(red: 0.56862745, green: 0.0, blue: 1.0, alpha: 1.0)
           break
            
        case "realities":
            self.labelTitle.textColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            self.viewColorCategory.backgroundColor = UIColor(red: 1.0, green: 0.34901961, blue: 0, alpha: 1.0)
            break
            
        case "séries":
            self.labelTitle.textColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)
            self.viewColorCategory.backgroundColor = UIColor(red: 0.02352941 , green: 0.49019608, blue: 1.0, alpha: 1.0)
            break
            
        case "teen":
            self.labelTitle.textColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            self.viewColorCategory.backgroundColor = UIColor(red: 0.02745098 , green: 0.61176471, blue: 0.18431373, alpha: 1.0)
            break
            
            
            
        default:
            self.labelTitle.textColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            self.viewColorCategory.backgroundColor = UIColor(red: 0.61860784, green: 0.0, blue: 0.16470588, alpha: 1.0)
            break
            
        }

            
        }
    
    }

