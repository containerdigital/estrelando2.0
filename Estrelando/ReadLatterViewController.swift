//
//  ReadLatterViewController.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/10/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class ReadLatterViewController: BaseRootViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let daoCoreData = ContentDaoCoreData()
    
    var results = [Content]()
    
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "ReadLatterTableViewCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: "cell")
        
        // COREDATA CALL
        if let resultsCD = daoCoreData.select() as? [ContentDto] {
            for item in resultsCD {
                print(item.categories)
                let content     = Content()
                content.id      = NSNumber(int: item.id)
                content.picture = UIImage(data: item.image!)
                content.foto1   = item.url
                content.title   = String.converString(item.title!.string)
                content.type    = "news"
                content.postDate = item.postDate

                results.append(content)
            }
        }
    }
    
    
    // MARK: - TABLEVIEW DELEGATES
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    // MARK: - TABLEVIEW DATASOURCE
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as? ReadLatterTableViewCell {
            
            let item = results[indexPath.row]
            
            cell.content  = item
            cell.viewController = self
            
            cell._image.image = item.picture
            cell._title.text  = item.title?.string
            cell._date.text   = item.postDate
            
            if let results = daoCoreData.select(item.id!.stringValue) as? [ContentDto] {
                if results.count > 0 {
                    cell._star.setImage(UIImage(named: "bt_read_latter_focused"), forState: .Normal)
                } else {
                    cell._star.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
                }
            } else {
                cell._star.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let tela = self.storyboard?.instantiateViewControllerWithIdentifier("internaNotas") as! InternaViewController
        tela.content = results[indexPath.row]
        tela.stringCategory = "celebridades"
        
        self.navigationController?.pushViewController(tela, animated: true)
    }

}
