//
//  MainHomeContent.swift
//  Estrelando
//
//  Created by iOS Developer on 09/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class MainHomeContent: Content {
    
    override init(dictionary: NSDictionary) {
        super.init()
        
        self.initAllKeys(dictionary)
    }
    
    override func initAllKeys(dictionary: NSDictionary) {
        
        if let id = dictionary.objectForKey("id") as? NSNumber {
            self.id = id
        }
        
        if let categories = dictionary.objectForKey("style") as? String{
            self.categories = categories
        }
        
        if let file = dictionary.objectForKey("Files") as? NSDictionary {
            
            if let foto1 = file.objectForKey("url") as? String {
            
                self.foto1 = foto1 //.resizeImage("230x220")

            }
            
        }
        
        if let link = dictionary.objectForKey("link") as? String{
            self.link = link
        }
        
//        if let postDate = dictionary.objectForKey("postDate") as? Int{
//            self.postDate = postDate
//        }
        
//        if let summary = dictionary.objectForKey("summary") as? String {
//            self.summary = summary
//        }
        
        if let title = dictionary.objectForKey("title") as? String{
            //self.title = title
            self.title = String.converString(title)
        }
        
        if let postText = dictionary.objectForKey("fullText") as? String{
            self.fullText = String.converString(postText)
        }
        
        if let type = dictionary.objectForKey("type") as? String{
            self.type = type
        }
        
        if let contentType = dictionary.objectForKey("contentType") as? String{
            self.contentType = contentType
        }
        
    }
    
}
