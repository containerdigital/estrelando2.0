//
//  EstrelandoAPI.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class EstrelandoAPI: NSObject {
    
    //
    static func homeHelper() -> Promise {
        let promise = Promise()
        let req = ServerHelper()
        let ret : Promise = req.request("http://app.estrelando.com.br/v1/home")
        
        var arrayContent = Array<Content>()
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    
                    if(json.count > 0) {
                        for i in 0..<json.count {
                            
                            let content = MainHomeContent(dictionary: json.objectAtIndex(i) as! NSDictionary)
                            arrayContent.append(content)
                            
                        }
                    }
                    
                    promise.resolv(arrayContent)
                } catch {
                    promise.rejects("Error")
                }
            }
        }
        return promise
    }
    
    //
    static func contentHelper(url: String) -> Promise {
        let promise = Promise()
        let req = ServerHelper()
        let ret: Promise = req.request(url)
        
        var arrayContent = Array<Content>()
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    
                    for i in 0..<json.count {
                        var news:Content!
                        news = Content(dictionary: json.objectAtIndex(i) as! NSDictionary)
                        arrayContent.append(news)
                    }
                    
                    promise.resolv(arrayContent)
                }   catch {
                    promise.rejects("Error")
                }
                
            }
        }
        
        return promise
    }
    
    //
    static func tagsHelper(url: String) -> Promise {
        let promise = Promise()
        let req = ServerHelper()
        let ret: Promise = req.request(url)
        
        var arrayContent = Array<Content>()
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    
                    for i in 0..<json.count {
                        var news:Content!
                        news = Profile(dictionary: json.objectAtIndex(i) as! NSDictionary)
                        arrayContent.append(news)
                    }
                    
                    promise.resolv(arrayContent)
                }   catch {
                    promise.rejects("Error")
                }
                
            }
        }
        
        return promise
    }
    
    //
    static func tagInside(url:String)->Promise{
        let promise = Promise()
        let req = ServerHelper()
        let ret:Promise = req.request(url)
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    let tag = TagProfile(dictionary: json)
   
                    promise.resolv(tag)
                }   catch {
                    promise.rejects("Error")
                }
            }
        }
        return promise
    }
    
    static func galleryInside(url:String) -> Promise {
        let promise = Promise()
        let req = ServerHelper()
        let ret:Promise = req.request(url)
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    let gallery = GalleryModel(dictionary: json)
                    
                    
                    
                    promise.resolv(gallery)
                }   catch {
                    promise.rejects("Error")
                }
            }
        }
        return promise
    }
    
    static func rankingInside(url:String) -> Promise {
        let promise = Promise()
        let req = ServerHelper()
        let ret:Promise = req.request(url)
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
                    let ranking = Ranking(dictionary: json)
                    
                    promise.resolv(ranking)
                }   catch {
                    promise.rejects("Error")
                }
            }
        }
        return promise
    }
    
    static func search(textSearch: String, page: Int, perPage: Int) -> Promise {
        let promise = Promise()
        let req = ServerHelper()
        let ret: Promise = req.request("http://app.estrelando.com.br/v1/content?q=\(textSearch)&page=\(page)&perPage=\(perPage)")
        
        var arrayContent = Array<Content>()
        
        ret.then { () -> Void in
            if let res = ret.Result as? NSData{
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(res, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                    
                    for i in 0..<json.count {
                        var content:Content!
                        content = Content(dictionary: json.objectAtIndex(i) as! NSDictionary)
                        arrayContent.append(content)
                    }
                    
                    promise.resolv(arrayContent)
                }   catch {
                    promise.rejects("Error")
                }
                
            }
        }
        
        return promise
    }

}
    

