//
//  Ranking.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/8/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import Foundation

class Ranking: NSObject {
    
    var _id: NSNumber?
    var _title: NSAttributedString?
    var _intro: NSAttributedString?
    var _type: String?
    var _publishedAt: String?
    var _link: String?
    var _arrImage = [RankingImage]()
    
    override init() {
        _id = nil
        _title = nil
        _intro = nil
        _type = nil
        _publishedAt = nil
        _link = nil
    }
    
    init(dictionary: NSDictionary) {
        if let id = dictionary["id"] as? NSNumber {
            _id = id
        }
        
        if let title = dictionary["title"] as? String {
            _title = String.converString(title)
        }
        
        if let intro = dictionary["intro"] as? String {
            _intro = String.converString(intro)
        }
        
        if let type = dictionary["type"] as? String {
            _type = type
        }
        
        if let publishedAt = dictionary["publishedAt"] as? String {
            let date : NSDate = NSDate.initFromString(publishedAt, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")!
            _publishedAt = date.getDate()
        }
        
        if let link = dictionary["link"] as? String {
            _link = link
        }
        
        if let arr = dictionary.objectForKey("Files") as? NSArray {
            for item in arr {
                if item["type"] as! String != "THUMBNAIL" {
                    let rImage = RankingImage(dictionary: item as! NSDictionary)
                    _arrImage.append(rImage)
                }
            }
        }
        
    }
    
}
