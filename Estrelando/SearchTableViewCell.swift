//
//  SearchTableViewCell.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/10/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var _image: UIImageView!
    @IBOutlet weak var _title: UILabel!
    @IBOutlet weak var _date: UILabel!
    @IBOutlet weak var _star: UIButton!
    
    let daoCoreData = ContentDaoCoreData()
    
    var content: Content!
    var viewController: SearchViewController!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func isNews() {
        if content.type == "news" {
            _star.hidden = false
            
            if let results = daoCoreData.select(content.id!.stringValue) as? [ContentDto] {
                if results.count > 0 {
                    _star.setImage(UIImage(named: "bt_read_latter_focused"), forState: .Normal)
                } else {
                    _star.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
                }
            } else {
                _star.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
            }
            
        } else {
            _star.hidden = true
        }
    }
    
    @IBAction func saveOrDelete(sender: AnyObject) {
        if let result = daoCoreData.select(content!.id!.stringValue) as? [ContentDto] {
            if result.count > 0 {
                let alert = UIAlertController(title: "", message: "A matéria será apagada do seu celular, você tem certeza?", preferredStyle: .Alert)
                
                alert.addAction(UIAlertAction(title: "Não", style: .Cancel, handler: nil))
                
                let deleteAction = UIAlertAction(title: "Sim", style: .Default, handler: { Void in
                    if self.daoCoreData.delete(result[0]) {
                        self._star.setImage(UIImage(named: "bt_read_latter"), forState: .Normal)
                    }
                })
                
                alert.addAction(deleteAction)
                
                viewController!.presentViewController(alert, animated: true, completion: nil)
            } else {
                content?.fullContent().then {
                    let entity = self.daoCoreData.entityObject() as! ContentDto
                    
                    entity.id = self.content!.id!.intValue
                    entity.categories = self.content?.categories
                    entity.image = UIImagePNGRepresentation(self.content!.picture!)
                    entity.postDate = self.content?.postDate
                    entity.summary = self.content?.summary
                    entity.type = self.content?.type
                    entity.contentType = self.content?.contentType
                    entity.link = self.content?.link
                    entity.title = self.content?.title
                    entity.postText = self.content?.postText
                    entity.fullText = self.content?.fullText
                    
                    if self.daoCoreData.insert(entity) {
                        self._star.setImage(UIImage(named: "bt_read_latter_focused"), forState: .Normal)
                    }
                }
            }
        }
    }
    
}
