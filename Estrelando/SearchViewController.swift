//
//  SearchViewController.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/10/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit
import SDWebImage

class SearchViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var searchCancel: UIBarButtonItem!
    
    @IBOutlet weak var tableView: UITableView!
    
    let cachePicture = NSMutableDictionary()
    
    var resultsSearch = Array<Content>()
    var page = 1
    var perPage = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Navigation
        self.navigationItem.hidesBackButton = true
        
        // Show keyboard when view will appear
        self.searchField.becomeFirstResponder()
        self.searchField.delegate = self
        
        // TableView Settings
        let nib = UINib(nibName: "SearchTableViewCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: "cell")
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "loadingCell")
    }
    
    func callAPI(textSearch: String) {
        self.resultsSearch.removeAll()
        self.page = 1
        
        let promise: Promise = EstrelandoAPI.search(textSearch, page: self.page, perPage: self.perPage)
        promise.then {
            self.page = self.page + 1
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.resultsSearch.appendContentsOf(promise.Result as! Array<Content>)
                self.tableView?.reloadData()
            }
        }
    }

    
    // MARK: UITABLEVIEW DELEGATES

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsSearch.count + 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95
    }
    
    
    // MARK: UITABLEVIEW DATASOURCE
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.item < self.resultsSearch.count {
            return cellForRow(tableView, indexPath: indexPath)
        } else {
            if self.page < 11 && self.page != 1 && self.searchField.text != "" {
                self.fetchMoreItens(self.searchField.text!)
                return loadingCellForIndexPath(indexPath)
            } else {
                return UITableViewCell()
            }
        }
    }
    
    // Content of cell
    func cellForRow(tableView: UITableView, indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as? SearchTableViewCell {
            let item = self.resultsSearch[indexPath.row]
            
            cell.content = item
            cell.viewController = self
            cell.isNews()
            
            cell._title.text = item.title?.string
            cell._date.text  = item.postDate
            
            //Download de imagem
            if (item.foto1 != nil && item.id != nil) {
                if let url = NSURL(string: item.foto1!) {
                    cell._image.sd_setImageWithURL(url, placeholderImage: UIImage(named: "default_image"), options: SDWebImageOptions.RefreshCached) {
                        img, error, _, _ in
                        
                        item.picture = img
                    }
                }
            }

            return cell
        }
        
        return UITableViewCell()
    }
    
    // Loading of cell
    func loadingCellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("loadingCell", forIndexPath: indexPath)
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.center = cell.center
        
        cell.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        return cell
    }
    
    func fetchMoreItens(textSearch: String) {
        let promise: Promise = EstrelandoAPI.search(textSearch, page: self.page, perPage: self.perPage)
        promise.then {
            self.page = self.page + 1
            
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.resultsSearch.appendContentsOf(promise.Result as! Array<Content>)
                self.tableView?.reloadData()
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < self.resultsSearch.count {
            let item = self.resultsSearch[indexPath.row]
            
            switch item.type! {
            case "news":
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("internaNotas") as? InternaViewController {
                    vc.content = item
                    vc.stringCategory = "celebridades"
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
                
            case "gallery":
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("galleryViewController") as? GalleryViewController {
                    vc.content = item
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
                
            case "ranking":
                if let vc = self.storyboard?.instantiateViewControllerWithIdentifier("rankingViewController") as? RankingViewController {
                    vc.content = item
                    vc.categorie = "celebridades"
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                break
                
            default:
                break
            }
        }
    }
    
    
    // MARK: UITEXTVIEW DELEGATE
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.searchField.resignFirstResponder()

        return true
    }
    
    
    // MARK: IBACTIONS
    
    // Busca assíncrona de acordo com a mudança de texto.
    @IBAction func assyncSearch(sender: AnyObject) {
        if self.searchField.text != "" {
            self.callAPI(self.searchField.text!)
        }
    }
    
    // Busca pelo botão
    @IBAction func searchAction(sender: AnyObject) {
        self.callAPI(self.searchField.text!)
    }

    @IBAction func cancelAction(sender: AnyObject) {
        //set keyboard hide when view will disappear
        self.searchField.resignFirstResponder()
        
        //dismiss view
        self.navigationController?.popViewControllerAnimated(true)
    }

}
