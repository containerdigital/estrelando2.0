//
//  DaoFactoryCoreData.swift
//  KindMe
//
//  Created on 15/07/15.
//  Copyright (c) 2015 KindMe. All rights reserved.
//

import CoreData
import UIKit

class DaoFactoryCoreData {
    
    private var context : NSManagedObjectContext?
    private var fetch : NSFetchRequest?
    
    init(){
        self.context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        self.fetch = NSFetchRequest()
    }
    
    func getContext() -> NSManagedObjectContext{
        return self.context!
    }
    
    func getFetch() -> NSFetchRequest{
        return self.fetch!
    }
    
    func setFetch(fr : NSFetchRequest){
        self.fetch = fr;
    }
}
