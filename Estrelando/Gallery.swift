//
//  Gallery.swift
//  Estrelando
//
//  Created by iOS Developer on 28/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class Gallery: Content {
        var photos:Array<GalleryFoto> = Array<GalleryFoto>()
    /*
    override init(dictionary: NSDictionary) {
        super.init(dictionary: dictionary)
        let array:NSArray = (dictionary.objectForKey("photos") as? NSArray)!
        for(var i = 0; i < array.count; i++){
            let item = GalleryFoto(dictionary: array[i] as! NSDictionary)
            self.photos.append(item)
            
        }
    }
    */
    override init(link:String){
        super.init(link: link)
        self.link = link
    }
    
    override func initAllKeys(dictionary: NSDictionary) {
        super.initAllKeys(dictionary)
        let array:NSArray = (dictionary.objectForKey("photos") as? NSArray)!
        for i in 0..<array.count {
            let item = GalleryFoto(dictionary: array[i] as! NSDictionary)
            self.photos.append(item)
            
        }

    }
    
}
