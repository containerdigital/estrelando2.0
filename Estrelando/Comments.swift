//
//  Comments.swift
//  Estrelando
//
//  Created by Thiago Harada on 6/14/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class Comments: NSObject {
    var created_time : String?
    var fromName : String?
    var fromId : String?
    var message : String?
    var id : String?

    override init() {
        
    }
    
    init(dictionary:NSDictionary){
        super.init()
        self.initAllKeys(dictionary)
    }
    
    func initAllKeys(dictionary: NSDictionary){
        
        if let created_time = dictionary.objectForKey("created_time") as? String {
            self.created_time = created_time
        }
        
		
		
        if let from = dictionary.objectForKey("from") as? NSDictionary {
            if let name = from["name"]{
                self.fromName = name as? String
            }
            if let id = from["id"]{
                self.fromId = id as? String
            }
        }

        if let message = dictionary.objectForKey("message") as? String {
            self.message = message
        }

        if let id = dictionary.objectForKey("id") as? String {
            self.id = id
        }

    }


}
