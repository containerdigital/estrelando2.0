//
//  CommentsViewController.swift
//  Estrelando
//
//  Created by Thiago Harada on 6/13/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class CommentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberOfComments: UILabel!
    @IBOutlet weak var addCommentTextField: UITextField!
    
    var content: Content?
    var facebookID: String?
    var arrComments = [Comments]()
    let token: String = "1210220309022976|FvCASalc_kZfCXFI151bykBGQ74"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NAVIGATION
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo-estrelando"))
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        self.tableView.separatorColor = UIColor.lightGrayColor()
        
        self.addCommentTextField.delegate = self
        
        var newUrl = self.content!.link!
        newUrl = newUrl.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
        print(newUrl)
        self.getFacebookIdForLink(newUrl)
    }
    
    //MARK: - Facebook Request Methods
    
    func getFacebookIdForLink(link: String){
        
        let paramsID = ["fields":"id, og_object"]
        let requestID: FBSDKGraphRequest = FBSDKGraphRequest.init(graphPath: link, parameters: paramsID, tokenString: self.token, version: "v2.6", HTTPMethod: "GET")
        requestID.startWithCompletionHandler({(connection, result, error) -> Void in
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                if let object = result.valueForKey("og_object") {
                    self.facebookID = object.valueForKey("id") as? String
                    print("FACEBOOK")
                    print(self.facebookID)
                    
                    let requestComments: FBSDKGraphRequest = FBSDKGraphRequest.init(graphPath: "/\(self.facebookID!)/comments", parameters: nil, tokenString: self.token, version: "v2.6", HTTPMethod: "GET")
                    requestComments.startWithCompletionHandler({(connection, result, error) -> Void in
                        if ((error) != nil)
                        {
                            // Process error
                            print("Error: \(error)")
                        }
                        else
                        {
                            if let arrObjects = result["data"] as? NSArray{
                                for obj in arrObjects {
                                    var comment: Comments!
                                    comment = Comments(dictionary: obj as! NSDictionary)
                                    print(comment)
                                    self.arrComments.append(comment)
                                }
                                
                                self.titleLabel.text = "\(self.content!.title!.string)"
                                self.numberOfComments.text = "\(self.arrComments.count) COMENTÁRIO(S)"
                                self.tableView.reloadData()
                                
                            }
                        }
                    })
                }
            }
        })
        
    }
    
    //MARK: - TableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrComments.count
    }
    
    //MARK: - TableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("commentCell", forIndexPath: indexPath) as! CommentsTableViewCell
        
        let stringFoto = "https://graph.facebook.com/\(self.arrComments[indexPath.item].fromId!)/picture?type=large"
        
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        var createdTimeDate = dateFormatter.dateFromString( self.arrComments[indexPath.item].created_time! )
        
        // Replace the hour (time) of both dates with 00:00
        createdTimeDate = calendar.startOfDayForDate(createdTimeDate!)
        let currentDate = calendar.startOfDayForDate(NSDate())
        
        let days: Int = daysBetweenDates(createdTimeDate!, endDate: currentDate)
        
        cell.nameLabel.text = self.arrComments[indexPath.item].fromName
        cell.commentLabel.text = self.arrComments[indexPath.item].message
        cell.dateLabel.text = "\(days) day(s)"
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            let url = NSURL(string: stringFoto)
            let dados = NSData(contentsOfURL: url!)
            let image = UIImage(data: dados!)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                cell.profilePictureImage.image = image
            })
        })

        
        return cell
    }
    
    func daysBetweenDates(startDate: NSDate, endDate: NSDate) -> Int
    {
        let calendar = NSCalendar.currentCalendar()
        
        let components = calendar.components([.Day], fromDate: startDate, toDate: endDate, options: [])
        
        return components.day
    }
    
    @IBAction func dismissView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: TEXTFIELD DELEGATE
    
    func dismissKeyboar(){
        //self.tableView.endEditing(true)
        self.addCommentTextField.endEditing(true)
    }
    
    func keyboardWillShow(notification:NSNotification) {
        let info:NSDictionary = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        UIView.animateWithDuration(0.25, delay: 0.25, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            self.view.frame = CGRectMake(0, (self.view.frame.origin.y - keyboardSize.height), self.view.frame.size.width, self.view.frame.size.height)
            }, completion: nil)
    }
    
    func keyboardWillHide(notification:NSNotification) {
        UIView.animateWithDuration(0.25, delay: 0.25, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            self.view.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height)
            }, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        //VERIFICAR SE ESTÁ LOGADO NO FACEBOOK ok
        // SE SIM, ENVIAR COMENTÁRIO
        // SE NAO, ABRIR CONECTAR NO FACEBOOK
        // logInWithPublishPermissions

        self.loginOnFacebook()
        
        dismissKeyboar()
        
        return true
        
    }
    
    func loginOnFacebook(){
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logOut()
        if (FBSDKAccessToken.currentAccessToken() == nil){
            login.logInWithPublishPermissions(["publish_actions","publish_pages"], fromViewController: self, handler: { (response:FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
                if(error != nil){
                    // Handle error
                    print("Error: \(error)")
                }
                else if(response.isCancelled){
                    // Authorization has been canceled by user
                    print("Cancelado!")
                }
                else {
                    // Authorization successful
                    //print(FBSDKAccessToken.currentAccessToken())
                    // no longer necessary as the token is already in the response
                    print("RESPONSE TOKEN = \(response.grantedPermissions)")
//                    print(response)
                    self.publishFacebookComment(self.addCommentTextField.text!)
                    
                }
            })
        } else {
            self.publishFacebookComment(self.addCommentTextField.text!)
        }
    }
    
    func publishFacebookComment(comment: String){
        let params: NSDictionary = ["message":comment]
        
        let accessToken: String = FBSDKAccessToken.currentAccessToken().tokenString!
        
        let requestPostComments: FBSDKGraphRequest = FBSDKGraphRequest.init(graphPath: "/\(self.facebookID!)/comments", parameters: params as [NSObject : AnyObject], tokenString: accessToken, version: "v2.6", HTTPMethod: "POST")
        
        requestPostComments.startWithCompletionHandler({(connection, result, error) -> Void in
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Seu comentário foi enviado com sucesso e será publicado após avaliação. ", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                self.addCommentTextField.text = ""
                
            }
        })
        

    }

}
