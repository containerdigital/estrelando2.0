//
//  TopCollectionViewDetailCell.swift
//  Estrelando
//
//  Created by Renato Mendes on 6/20/16.
//  Copyright © 2016 Contrainer Digital. All rights reserved.
//

import UIKit

class TopCollectionViewDetailCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var category: UILabel!
    
}
