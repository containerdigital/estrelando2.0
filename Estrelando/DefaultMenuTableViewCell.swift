//
//  DefaultMenuTableViewCell.swift
//  testeMenuEstrelando
//
//  Created by Renato Mendes on 6/1/16.
//  Copyright © 2016 Renato Mendes. All rights reserved.
//

import UIKit

class DefaultMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var _title: UILabel!
    @IBOutlet weak var _icon: UIImageView!
    @IBOutlet weak var _separator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func colorForCategory (category: String) -> UIColor {
        switch category {
        case "CELEBRIDADES":
            return UIColor(red: 251.0/255, green: 0.0/255, blue: 57.0/255, alpha: 1.0)
        case "ESTILO":
            return UIColor(red: 120.0/255, green: 0.0/255, blue: 213.0/255, alpha: 1.0)
        case "REALITIES":
            return UIColor(red: 230.0/255, green: 106.0/255, blue: 0.0/255, alpha: 1.0)
        case "SÉRIES":
            return UIColor(red: 13.0/255, green: 100.0/255, blue: 205.0/255, alpha: 1.0)
        case "TEEN":
            return UIColor(red: 17.0/255, green: 124.0/255, blue: 28.0/255, alpha: 1.0)
        default:
            return UIColor(red: 251.0/255, green: 0.0/255, blue: 57.0/255, alpha: 1.0)
        }
    }
    
}
