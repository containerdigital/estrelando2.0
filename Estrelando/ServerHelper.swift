//
//  ServerHelper.swift
//  Estrelando
//
//  Created by iOS Developer on 07/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

protocol ServerHelperDelegate : NSObjectProtocol {
    func requestEnded(data:NSData)
}


class ServerHelper: NSObject, DownloaderDelegate {
    
    let downloader = Downloader()
    var task = NSURLSessionDataTask()
    var delegate:ServerHelperDelegate?
    var updatePending = Array<Downloader>()
    var recentUpdate = false
    
    func request(page:String)->Promise {
        let promise = Promise()
        let url = NSURL(string: page)
        
        if(url == nil){
            promise.rejects("INVALID_URL")
            return promise
        }
        
        self.downloader.delegate = self
        self.downloader.initDownload(url!)
        self.downloader.context = promise
        return promise
    }
    
    func requestToPost(page:String, context:AnyObject,HttpBody:[AnyObject])->Promise{
        let promise = Promise()
        let url = NSURL(string: page)
        
        if(url == nil){
            promise.rejects("INVALID_URL")
            return promise
        }
        
        /*Post values , of an request.
            req.sendPOSTValures(HttpBody, withBondaryString: AnyString)
        */
        
        self.downloader.delegate = self
        self.downloader.initDownload(url!)
        self.downloader.context = promise
        return promise
    }
    
    //Download de imagem Assync utilizando data task e promiseD
    func downloadPicture(url:String)->Promise{
        return self.request(url)
    }
    
    //MARK: DOWNLOADER DELEGATES
    
    //Quando o download acabar adiciona o data a promise
    func downloaderDidFinnish(contexto: AnyObject?, download: Downloader, dados: NSData) {
        let promise = contexto as! Promise
        promise.resolv(dados)
    }

}
