//
//  Profiles.swift
//  Estrelando
//
//  Created by iOS Developer on 30/10/15.
//  Copyright © 2015 Container Digital. All rights reserved.
//

import UIKit

class Profile: Content {
    
    var thumb:UIImage?
    
    
    func initAllKeyOfProfiles(dictionary:NSDictionary){
        if let id = dictionary.objectForKey("id") as? NSNumber{
            self.id = id
        }
        if let link = dictionary.objectForKey("link") as? String{
            self.link = link
        }
        if let title = dictionary.objectForKey("title") as? String{
            self.title = String.converString(title)
        }
        if let thumb = dictionary.objectForKey("thumb") as? String{
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                
                let serverHelper = ServerHelper()
                let promise = serverHelper.downloadPicture(thumb)
                
                promise.then({ [weak self]  in
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        if let result = promise.Result as? NSData{
                            let picture = UIImage(data: result)
                            self!.thumb = picture
                        }
                        
                    });
                })
                
            });

        }
    }
    
}
